from g4epy import Geant4Eic


def run_g4e(input_file, output_name, nevents, e_energy, i_energy, decay_level=1):

    g4e = Geant4Eic(beamline='erhic')

    # Set beam energies
    g4e.command([f'/detsetup/eBeam {e_energy}',
                 f'/detsetup/pBeam {i_energy}'
                 ])

    # To control how many generation of secondaries (tracks and their hits) to save,
    # there is a configuration:
    #    /rootOutput/saveSecondaryLevel <ancestry-level>
    #
    # <ancestry-level> sets 0-n levels of ancestry which are saved in root file.
    #
    # Example:
    #
    # -1 - save everything
    # 0 - save only primary particles
    # 1 - save primaries and their daughters
    # 2 - save primaries, daughters and daughters’ daughters
    # n - save n generations of secondaries
    #
    # (primaries - particles that came from a generator/input file)
    #
    # The default level is 3, which corresponds to:
    #
    # /rootOutput/saveSecondaryLevel 3
    #
    # We set it to 1. If only vertex particles are of the interest, set it to 0
    g4e.command([f'/rootOutput/saveSecondaryLevel {decay_level}'])
    g4e.command([f'/g4e/tracking/saveSecondaryLevel {decay_level}'])


    # Extension is ommited here
    # g4e creates a bunch of files with this name and different extensions
    g4e.output(output_name)

    # Run g4e run!!!
    g4e.source(input_file) \
        .beam_on(nevents) \
        .run()


def parse_event_count(file_name):
    bufsize = 1000000
    event_counter = 0

    with open(file_name) as infile:
        while True:
            lines = infile.readlines(bufsize)
            if not lines:
                break
            for i, line in enumerate(lines):
                if len(line.split()) == 10:
                    event_counter += 1
    return event_counter


"""
'OUTPUTS/g4e-lvl1-k_lambda_10on100'

# Run g4e run!!!
g4e.source('/w/general-scifs17exp/eic/mc/meson_MC/OUTPUTS/k_lambda_10on100_lund.dat') \
"""

# The files look like this
# /home/romanov/eic/data/k_lambda_5on41_lund.dat
# /home/romanov/eic/data/k_lambda_5on100_lund.dat
# /home/romanov/eic/data/k_lambda_10on100_lund.dat
# /home/romanov/eic/data/k_lambda_18on275_lund.dat


#energy_pairs = [(5, 41), (5, 100), (10, 100), (10, 135)]
#energy_pairs = [(5, 41), (5, 100), (10, 100)]
energy_pairs = [(18, 275)]

for e_energy, i_energy in energy_pairs:

    input_file_name = '/home/romanov/eic/data/k_lambda_{}on{}_x0.001-1.000_q1.0-100.0_lund.dat'.format(e_energy, i_energy)
    output_name = 'OUTPUTS/g4e_k_lambda_{}on{}_all_l3'.format(e_energy, i_energy)
    nevents = parse_event_count(input_file_name)

    # Pretty printing the results
    print(f"\n\n DO ENERGIES {e_energy} on {i_energy}")
    print("====================================")
    print(f"input_file_name = {input_file_name}")
    print(f"output_name     = {output_name}")
    print(f"nevents         = {nevents}")
    print("\n\n\n")

    # Run processing
    run_g4e(input_file=input_file_name,
            output_name=output_name,
            nevents= nevents, # 10000
            e_energy=e_energy,
            i_energy=i_energy,
            decay_level=3)


