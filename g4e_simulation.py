from g4epy import Geant4Eic

g4e = Geant4Eic(beamline='erhic')

# Set beam energies
g4e.command(['/eic/refdet/eBeam 18',
             '/eic/refdet/pBeam 275'
             ])

# To control how many generation of secondaries (tracks and their hits) to save,
# there is a configuration:
#    /rootOutput/saveSecondaryLevel <ancestry-level>
#
# <ancestry-level> sets 0-n levels of ancestry which are saved in root file.
#
# Example:
#
# -1 - save everything
# 0 - save only primary particles
# 1 - save primaries and their daughters
# 2 - save primaries, daughters and daughters’ daughters
# n - save n generations of secondaries
#
# (primaries - particles that came from a generator/input file)
#
# The default level is 3, which corresponds to:
#
# /rootOutput/saveSecondaryLevel 3
#
# We set it to 1. If only vertex particles are of the interest, set it to 0
g4e.command(['/eic/rootOutput/saveSecondaryLevel 3'])

g4e.command(['/eic/tracking/saveSecondaryLevel 3'])

# Extension is omitted here
# g4e creates a bunch of files with this name and different extensions
g4e.output('OUTPUTS/g4e_k_lambda_18on275_all_l3')

# Run g4e run!!!
g4e.source('/home/romanov/eic/data/k_lambda_10on275_x0.001-1.000_q1.0-100.0_lund.dat') \
   .beam_on(10000) \
   .run()