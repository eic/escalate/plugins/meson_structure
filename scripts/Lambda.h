//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Sat Jul  4 16:58:24 2020 by ROOT version 6.16/00
// from TTree events/Flattened root tree with event data
// found on file: g4e_output_lambda_10x100.root
//////////////////////////////////////////////////////////

/*   usage:

.L Lambda.C
Lambda t("g4e_lambda-18x275output.root");
t.Loop(); 

*/

#ifndef Lambda_h
#define Lambda_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include "TDatabasePDG.h"

// Header file for the classes stored in the TTree if any.
#include "vector"
#include "vector"
#include "vector"
#include "vector"

class Lambda {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   ULong64_t       event_id;
   ULong64_t       hit_count;
   vector<unsigned long> *hit_id;
   vector<unsigned long> *hit_trk_id;
   vector<unsigned long> *hit_ptr_id;
   vector<unsigned long> *hit_parent_trk_id;
   vector<string>  *hit_vol_name;
   vector<double>  *hit_x;
   vector<double>  *hit_y;
   vector<double>  *hit_z;
   vector<unsigned long> *hit_i_rep;
   vector<unsigned long> *hit_j_rep;
   vector<double>  *hit_e_loss;
   ULong64_t       trk_count;
   vector<unsigned long> *trk_id;
   vector<long>    *trk_pdg;
   vector<unsigned long> *trk_parent_id;
   vector<long>    *trk_create_proc;
   vector<long>    *trk_level;
   vector<double>  *trk_vtx_x;
   vector<double>  *trk_vtx_y;
   vector<double>  *trk_vtx_z;
   vector<double>  *trk_vtx_dir_x;
   vector<double>  *trk_vtx_dir_y;
   vector<double>  *trk_vtx_dir_z;
   vector<double>  *trk_mom;
   ULong64_t       gen_prt_count;
   vector<unsigned long> *gen_prt_id;
   vector<unsigned long> *gen_prt_vtx_id;
   vector<unsigned long> *gen_prt_pdg;
   vector<unsigned long> *gen_prt_trk_id;
   vector<double>  *gen_prt_charge;
   vector<double>  *gen_prt_dir_x;
   vector<double>  *gen_prt_dir_y;
   vector<double>  *gen_prt_dir_z;
   vector<double>  *gen_prt_tot_mom;
   vector<double>  *gen_prt_tot_e;
   vector<double>  *gen_prt_time;
   vector<double>  *gen_prt_polariz_x;
   vector<double>  *gen_prt_polariz_y;
   vector<double>  *gen_prt_polariz_z;
   ULong64_t       gen_vtx_count;
   vector<unsigned long> *gen_vtx_id;
   vector<unsigned long> *gen_vtx_part_count;
   vector<double>  *gen_vtx_x;
   vector<double>  *gen_vtx_y;
   vector<double>  *gen_vtx_z;
   vector<double>  *gen_vtx_time;
   vector<double>  *gen_vtx_weight;

   // List of branches
   TBranch        *b_event_id;   //!
   TBranch        *b_hit_count;   //!
   TBranch        *b_hit_id;   //!
   TBranch        *b_hit_trk_id;   //!
   TBranch        *b_hit_ptr_id;   //!
   TBranch        *b_hit_parent_trk_id;   //!
   TBranch        *b_hit_vol_name;   //!
   TBranch        *b_hit_x;   //!
   TBranch        *b_hit_y;   //!
   TBranch        *b_hit_z;   //!
   TBranch        *b_hit_i_rep;   //!
   TBranch        *b_hit_j_rep;   //!
   TBranch        *b_hit_e_loss;   //!
   TBranch        *b_trk_count;   //!
   TBranch        *b_trk_id;   //!
   TBranch        *b_trk_pdg;   //!
   TBranch        *b_trk_parent_id;   //!
   TBranch        *b_trk_create_proc;   //!
   TBranch        *b_trk_level;   //!
   TBranch        *b_trk_vtx_x;   //!
   TBranch        *b_trk_vtx_y;   //!
   TBranch        *b_trk_vtx_z;   //!
   TBranch        *b_trk_vtx_dir_x;   //!
   TBranch        *b_trk_vtx_dir_y;   //!
   TBranch        *b_trk_vtx_dir_z;   //!
   TBranch        *b_trk_mom;   //!
   TBranch        *b_gen_prt_count;   //!
   TBranch        *b_gen_prt_id;   //!
   TBranch        *b_gen_prt_vtx_id;   //!
   TBranch        *b_gen_prt_pdg;   //!
   TBranch        *b_gen_prt_trk_id;   //!
   TBranch        *b_gen_prt_charge;   //!
   TBranch        *b_gen_prt_dir_x;   //!
   TBranch        *b_gen_prt_dir_y;   //!
   TBranch        *b_gen_prt_dir_z;   //!
   TBranch        *b_gen_prt_tot_mom;   //!
   TBranch        *b_gen_prt_tot_e;   //!
   TBranch        *b_gen_prt_time;   //!
   TBranch        *b_gen_prt_polariz_x;   //!
   TBranch        *b_gen_prt_polariz_y;   //!
   TBranch        *b_gen_prt_polariz_z;   //!
   TBranch        *b_gen_vtx_count;   //!
   TBranch        *b_gen_vtx_id;   //!
   TBranch        *b_gen_vtx_part_count;   //!
   TBranch        *b_gen_vtx_x;   //!
   TBranch        *b_gen_vtx_y;   //!
   TBranch        *b_gen_vtx_z;   //!
   TBranch        *b_gen_vtx_time;   //!
   TBranch        *b_gen_vtx_weight;   //!

   Lambda(string filename="", double Ebeam=100);
   virtual ~Lambda();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);


 //-----   
   //double MassPI, MassK, MassP, MassE, MassMU;   
   const double   MassPI=0.139570, MassK=0.493677, MassP=0.93827, MassN=0.93956, MassE=0.000511, MassMU=0.105658;   

  int Ntracks;
  std::vector<TLorentzVector> vGamma;
  std::vector<TLorentzVector> vProton;
  std::vector<TLorentzVector> vNeutron;
  std::vector<TLorentzVector> vPion;
  std::vector<TLorentzVector> vKaon;
  std::vector<int>            vCharge;
  std::vector<int>            vTrkID;
  std::vector<int>            vPDG;
  std::vector<TVector3>       vVtx;
  std::vector<TVector3>       vHit;
  std::vector<int>            vVol;


   TH1D *hcharge, *hcount;
   TH1F *hdmd, *hntrk;
   TH2F *h2,*hp2dedx, *hepfc, *hepbc, *h2dal, *h2em, *h2xq2;
   TH1F *hDst_mass, *hDst_mass1, *hDst_mass2, *hDst_pt, *hDst_dm, *hDst_dm2, *hD0_mass, *hPhi0_mass, *hD0_pt, *hNtracks;
   TH1F *hL0_mass, *hL0_mass_sm, *hL0_perp, *hL0_perp2, *hL0_perp_sm, *hL0_perp2_sm;
   TH1F *hLc_mass, *hDsbs_mass;
   TH2F *h2vtxptot1, *h2vtxptot2,*h2vtxtheta1, *h2vtxtheta2 ;
   TH1F *hPX[10], *hPY[10], *hPZ[10], *hPT[10], *hPTOT[10], *hPHI[10], *hETA[10],  *hTHETA[10];
   TH1F *hpx, *hpy, *hpz, *hpt, *hptot, *hphi, *heta,  *htheta,  *hdv1, *hdv2, *hdv3, *heph, *hq2, *hxbj, *hedis;
   TH1F *hpx1, *hpy1, *hpz1, *hpt1, *hptot1, *hphi1, *heta1,  *htheta1;
   TH1F *hpx2, *hpy2, *hpz2, *hpt2, *hptot2, *hphi2, *heta2,  *htheta2;
   TH1D *hcharge1,*hcharge2, *hCHARGE[10];
   TH1F *hK0s, *hee, *heef, *hmumu, *hmumuf, *hmm, *hmpt, *heopfc,*heopbc;
   TH2F *h2rpot1, *h2offmom1, *h2offmom11, *h2rpot2, *h2offmom2, *h2offmom22,  *h2negtrk1, *h2negtrk2, *h2B0trk1, *h2B0trk2, *h2ci_GEM, *h2ci_GEMp, *h2cb_CTD, *h2ce_GEM, *h2ci_HCAL, *h2corrz ; 
   TH1D *hXLp, *hXLpi, *hTHETAp, *hTHETApi;
    TFile* fout;

    int DSTARg4();
   int LambdaP();
   int LambdaN();
   void PFillx(int i, int ih);
   void PFill(int i);
   void PFill1(int i);
   void PFill2(int i);
   void Count(const char *tit);
   void Count(const char *tit, double cut1);
   void Count(const char *tit, double cut1, double cut2);

   double EPbeam, EEbeam;

   TRandom *random;

};

#endif

#ifdef Lambda_cxx
Lambda::Lambda(string filename, double EPbeami) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.

  EPbeam=EPbeami;
  

  TTree *tree;
  TFile *f=NULL ;
  if (filename=="")  {
    printf("open file default file \n");
    f = (TFile*)gROOT->GetListOfFiles()->FindObject("g4e_output_lambda_10x100.root");
  } else {
    printf("open file %s file \n",filename.c_str());
    f = (TFile*)gROOT->GetListOfFiles()->FindObject(filename.c_str());
  }
  if (!f || !f->IsOpen()) {
    f = new TFile(filename.c_str());
  }
  f->GetObject("events",tree);
 
   Init(tree);
}

Lambda::~Lambda()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t Lambda::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t Lambda::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void Lambda::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   hit_id = 0;
   hit_trk_id = 0;
   hit_ptr_id = 0;
   hit_parent_trk_id = 0;
   hit_vol_name = 0;
   hit_x = 0;
   hit_y = 0;
   hit_z = 0;
   hit_i_rep = 0;
   hit_j_rep = 0;
   hit_e_loss = 0;
   trk_id = 0;
   trk_pdg = 0;
   trk_parent_id = 0;
   trk_create_proc = 0;
   trk_level = 0;
   trk_vtx_x = 0;
   trk_vtx_y = 0;
   trk_vtx_z = 0;
   trk_vtx_dir_x = 0;
   trk_vtx_dir_y = 0;
   trk_vtx_dir_z = 0;
   trk_mom = 0;
   gen_prt_id = 0;
   gen_prt_vtx_id = 0;
   gen_prt_pdg = 0;
   gen_prt_trk_id = 0;
   gen_prt_charge = 0;
   gen_prt_dir_x = 0;
   gen_prt_dir_y = 0;
   gen_prt_dir_z = 0;
   gen_prt_tot_mom = 0;
   gen_prt_tot_e = 0;
   gen_prt_time = 0;
   gen_prt_polariz_x = 0;
   gen_prt_polariz_y = 0;
   gen_prt_polariz_z = 0;
   gen_vtx_id = 0;
   gen_vtx_part_count = 0;
   gen_vtx_x = 0;
   gen_vtx_y = 0;
   gen_vtx_z = 0;
   gen_vtx_time = 0;
   gen_vtx_weight = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("event_id", &event_id, &b_event_id);
   fChain->SetBranchAddress("hit_count", &hit_count, &b_hit_count);
   fChain->SetBranchAddress("hit_id", &hit_id, &b_hit_id);
   fChain->SetBranchAddress("hit_trk_id", &hit_trk_id, &b_hit_trk_id);
   fChain->SetBranchAddress("hit_ptr_id", &hit_ptr_id, &b_hit_ptr_id);
   fChain->SetBranchAddress("hit_parent_trk_id", &hit_parent_trk_id, &b_hit_parent_trk_id);
   fChain->SetBranchAddress("hit_vol_name", &hit_vol_name, &b_hit_vol_name);
   fChain->SetBranchAddress("hit_x", &hit_x, &b_hit_x);
   fChain->SetBranchAddress("hit_y", &hit_y, &b_hit_y);
   fChain->SetBranchAddress("hit_z", &hit_z, &b_hit_z);
   fChain->SetBranchAddress("hit_i_rep", &hit_i_rep, &b_hit_i_rep);
   fChain->SetBranchAddress("hit_j_rep", &hit_j_rep, &b_hit_j_rep);
   fChain->SetBranchAddress("hit_e_loss", &hit_e_loss, &b_hit_e_loss);
   fChain->SetBranchAddress("trk_count", &trk_count, &b_trk_count);
   fChain->SetBranchAddress("trk_id", &trk_id, &b_trk_id);
   fChain->SetBranchAddress("trk_pdg", &trk_pdg, &b_trk_pdg);
   fChain->SetBranchAddress("trk_parent_id", &trk_parent_id, &b_trk_parent_id);
   fChain->SetBranchAddress("trk_create_proc", &trk_create_proc, &b_trk_create_proc);
   fChain->SetBranchAddress("trk_level", &trk_level, &b_trk_level);
   fChain->SetBranchAddress("trk_vtx_x", &trk_vtx_x, &b_trk_vtx_x);
   fChain->SetBranchAddress("trk_vtx_y", &trk_vtx_y, &b_trk_vtx_y);
   fChain->SetBranchAddress("trk_vtx_z", &trk_vtx_z, &b_trk_vtx_z);
   fChain->SetBranchAddress("trk_vtx_dir_x", &trk_vtx_dir_x, &b_trk_vtx_dir_x);
   fChain->SetBranchAddress("trk_vtx_dir_y", &trk_vtx_dir_y, &b_trk_vtx_dir_y);
   fChain->SetBranchAddress("trk_vtx_dir_z", &trk_vtx_dir_z, &b_trk_vtx_dir_z);
   fChain->SetBranchAddress("trk_mom", &trk_mom, &b_trk_mom);
   fChain->SetBranchAddress("gen_prt_count", &gen_prt_count, &b_gen_prt_count);
   fChain->SetBranchAddress("gen_prt_id", &gen_prt_id, &b_gen_prt_id);
   fChain->SetBranchAddress("gen_prt_vtx_id", &gen_prt_vtx_id, &b_gen_prt_vtx_id);
   fChain->SetBranchAddress("gen_prt_pdg", &gen_prt_pdg, &b_gen_prt_pdg);
   fChain->SetBranchAddress("gen_prt_trk_id", &gen_prt_trk_id, &b_gen_prt_trk_id);
   fChain->SetBranchAddress("gen_prt_charge", &gen_prt_charge, &b_gen_prt_charge);
   fChain->SetBranchAddress("gen_prt_dir_x", &gen_prt_dir_x, &b_gen_prt_dir_x);
   fChain->SetBranchAddress("gen_prt_dir_y", &gen_prt_dir_y, &b_gen_prt_dir_y);
   fChain->SetBranchAddress("gen_prt_dir_z", &gen_prt_dir_z, &b_gen_prt_dir_z);
   fChain->SetBranchAddress("gen_prt_tot_mom", &gen_prt_tot_mom, &b_gen_prt_tot_mom);
   fChain->SetBranchAddress("gen_prt_tot_e", &gen_prt_tot_e, &b_gen_prt_tot_e);
   fChain->SetBranchAddress("gen_prt_time", &gen_prt_time, &b_gen_prt_time);
   fChain->SetBranchAddress("gen_prt_polariz_x", &gen_prt_polariz_x, &b_gen_prt_polariz_x);
   fChain->SetBranchAddress("gen_prt_polariz_y", &gen_prt_polariz_y, &b_gen_prt_polariz_y);
   fChain->SetBranchAddress("gen_prt_polariz_z", &gen_prt_polariz_z, &b_gen_prt_polariz_z);
   fChain->SetBranchAddress("gen_vtx_count", &gen_vtx_count, &b_gen_vtx_count);
   fChain->SetBranchAddress("gen_vtx_id", &gen_vtx_id, &b_gen_vtx_id);
   fChain->SetBranchAddress("gen_vtx_part_count", &gen_vtx_part_count, &b_gen_vtx_part_count);
   fChain->SetBranchAddress("gen_vtx_x", &gen_vtx_x, &b_gen_vtx_x);
   fChain->SetBranchAddress("gen_vtx_y", &gen_vtx_y, &b_gen_vtx_y);
   fChain->SetBranchAddress("gen_vtx_z", &gen_vtx_z, &b_gen_vtx_z);
   fChain->SetBranchAddress("gen_vtx_time", &gen_vtx_time, &b_gen_vtx_time);
   fChain->SetBranchAddress("gen_vtx_weight", &gen_vtx_weight, &b_gen_vtx_weight);


   //create histograms

  char mlpname[128];   sprintf(mlpname,"Lambda.root");
  fout = new TFile(mlpname,"RECREATE");

   hcount= new TH1D("hcount","Count",3,0,3);   hcount->SetStats(0);   hcount->SetFillColor(38);   hcount->SetMinimum(1.);
#if ROOT_VERSION_CODE > ROOT_VERSION(6,0,0)
   hcount->SetCanExtend(TH1::kXaxis);
#else
   hcount->SetBit(TH1::kCanRebin);
#endif
   hntrk = new TH1F("hntrk","Ntracks",40,-0.5,39.5);            
   hdmd  = new TH1F("hdmd","dm_d",40,0.13,0.17);                 
   h2    = new TH2F("h2","ptD0 vs dm_d",30,0.135,0.165,30,0.,5); 
   h2dal = new TH2F("h2dal","M_{1}^{2} vs M_{2}^{2}", 100, 1.7, 2.1, 100, 1.7, 2.1); 
   h2em  = new TH2F("h2em","MD^{*} vs E_{#g}",  100, 1.950 , 2.150, 100, 8., 12.); 
   //h2em  = new TH2F("h2em","Kin plot X , Q^{2}",  100, -4 , 0., 100, 0., 4.); 
   //h2em->GetXaxis()->SetTitle("log (X)");
   //h2em->GetYaxis()->SetTitle("log(Q^{2})");

   h2xq2  = new TH2F("h2xq2","Kin plot X , Q^{2}",  100, -3 , 0., 100, 0., 4.); 
   //h2xq2  = new TH2F("h2xq2","Kin plot X , Q^{2}",  100, -5 , 1., 100, 0.9, 1.4); 
   h2xq2->GetXaxis()->SetTitle("log (X)");
   h2xq2->GetYaxis()->SetTitle("log(Q^{2})");
   hxbj  = new TH1F("hxbj" ,"log(xBJ) "     , 100, -3.0   , 0.0  );
   hq2  = new TH1F("hq2" ,"log(Q2) "     , 100, 0.0   , 4.0  );
   hedis  = new TH1F("hedis" ,"E dis el, GeV"     , 100, 0.0 , 50.0  );

   char hnam[128];
   char htit[128];
   for (int i=0; i<10; i++) {
     sprintf(hnam,"hPX%d",i);      hPX[i]     = new TH1F(hnam,hnam , 200, -1.0   , 1.0  );
     sprintf(hnam,"hPY%d",i);      hPY[i]     = new TH1F(hnam,hnam , 200, -1.0   , 1.0  );
     sprintf(hnam,"hPZ%d",i);      hPZ[i]     = new TH1F(hnam,hnam   , 1000,  0.0   ,300.0  );
     sprintf(hnam,"hPT%d",i);     hPT[i]      = new TH1F(hnam,hnam  , 200,  0.0   , 1.0  );
     sprintf(hnam,"hPTOT%d",i);   hPTOT[i]    = new TH1F(hnam,hnam , 1000,  0.0   , 300.0  );
     sprintf(hnam,"hPHI%d",i);    hPHI[i]     = new TH1F(hnam,hnam , 200, -4.0  , +4.0  );
     sprintf(hnam,"hETA%d",i);    hETA[i]     = new TH1F(hnam,hnam , 200, -7.0  , +7.0  );
     sprintf(hnam,"hTHETA%d",i);  hTHETA[i]   = new TH1F(hnam,hnam, 200, -100  , +100  );
     sprintf(hnam,"hCHARGE%d",i); hCHARGE[i]  = new TH1D(hnam,hnam, 10, -5.5 , +4.5 );
     //h2vtxtheta2 = new TH2F("h2vtxtheta2" ," h2vtxtheta2", 100,  0.0  , 30.,100,0.,100.  );
   }

   hpx  = new TH1F("hpx" ,"All px "     , 200, -10.0   , 10.0  );
   hpy  = new TH1F("hpy" ,"All py "     , 200, -10.0   , 10.0  );
   hpz  = new TH1F("hpz" ,"All pz "     , 200,  0.0   ,300.0  );
   hpt  = new TH1F("hpt" ,"All pt "     , 200,  0.0   , 10.0  );
   hptot= new TH1F("hptot" ,"All ptot " , 200,  0.0   , 300.0  );
   hphi = new TH1F("hphi" ,"All phi "   , 200, -4.0  , +4.0  );
   heta = new TH1F("heta" ,"All eta "   , 200, -7.0  , +7.0  );
   htheta = new TH1F("htheta" ,"All theta " , 200, -100  , +100  );
   hcharge   = new TH1D("hcharge","Particles charge " , 10, -5.5 , +4.5 );

   hpx1  = new TH1F("hpx1" ,"All px "     , 200, -10.0   , 10.0  );
   hpy1  = new TH1F("hpy1" ,"All py "     , 200, -10.0   , 10.0  );
   hpz1  = new TH1F("hpz1" ,"All pz "     , 200,  0.0   ,300.0  );
   hpt1  = new TH1F("hpt1" ,"All pt "     , 200,  0.0   , 10.0  );
   hptot1= new TH1F("hptot1" ,"All ptot " , 200,  0.0   , 300.0  );
   hphi1 = new TH1F("hphi1" ,"All phi "   , 200, -4.0  , +4.0  );
   heta1 = new TH1F("heta1" ,"All eta "   , 200, -7.0  , +7.0  );
   htheta1 = new TH1F("htheta1" ,"All theta " , 200, -100  , +100  );
   hcharge1   = new TH1D("hcharge1","Particles charge " , 10, -5.5 , +4.5 );

   hpx2  = new TH1F("hpx2" ,"All px "     , 200, -10.0   , 10.0  );
   hpy2  = new TH1F("hpy2" ,"All py "     , 200, -10.0   , 10.0  );
   hpz2  = new TH1F("hpz2" ,"All pz "     , 200,  0.0   ,300.0  );
   hpt2  = new TH1F("hpt2" ,"All pt "     , 200,  0.0   , 10.0  );
   hptot2= new TH1F("hptot2" ,"All ptot " , 200,  0.0   , 300.0  );
   hphi2 = new TH1F("hphi2" ,"All phi "   , 200, -4.0  , +4.0  );
   heta2 = new TH1F("heta2" ,"All eta "   , 200, -7.0  , +7.0  );
   htheta2 = new TH1F("htheta2" ,"All theta " , 200, -100  , +100  );
   hcharge2   = new TH1D("hcharge2","Particles charge " , 10, -5.5 , +4.5 );

   hdv1 = new TH1F("hdv1" ,"diff vtx 1 ", 100,  0.0  , +3.0  );
   hdv2 = new TH1F("hdv2" ,"diff vtx 2 ", 100,  0.0  , +3.0  );
   hdv3 = new TH1F("hdv3" ,"diff vtx 3 ", 100,  0.0  , +3.0  );

   gStyle->SetStatH(0.05);
   gROOT->ForceStyle();

   h2vtxptot1 = new TH2F("h2vtxptot1" ,"proton, p_{tot} vs z_{vtx} ; z_{vtx}, [m]; p_{tot}, [GeV]", 100,  0.0  , 30.,100,0.,300.  );
   //h2vtxptot1->SetStats(0);
   h2vtxptot2 = new TH2F("h2vtxptot2" ,"#pi^{-},  p_{tot} vs z_{vtx} ; z_{vtx}, [m]; p_{tot}, [GeV]", 100,  0.0  , 30.,50,0.,100.  );
   //h2vtxptot2->SetStats(0);

   h2vtxtheta1 = new TH2F("h2vtxtheta1" ,"proton, #Theta vs z_{vtx} ; z_{vtx}, [m]; #Theta, [mrad]", 100,  0.0  , 30.,100,0.,20.  );
   //h2vtxtheta1->SetStats(0);
   h2vtxtheta2 = new TH2F("h2vtxtheta2" ,"#pi^{-}, #Theta vs z_{vtx} ; z_{vtx}, [m]; #Theta, [mrad]", 100,  0.0  , 30.,100,0.,30.  );
   //h2vtxtheta2->SetStats(0);

   gStyle->SetStatH(0.1);
   gROOT->ForceStyle();

   //------------- Occupancy plots in subdetectors -------
   h2rpot1 = new TH2F("h2rpot1" ,"h2rpot1", 100,  -0.0  , 1500.,100,-300.,300.  );
   h2offmom1 = new TH2F("h2offmom1" ,"h2offmom1", 100,  0.0, 1500.,300,-300.,300.  );
   h2offmom11 = new TH2F("h2offmom11" ,"h2offmom11", 100,  0.0, 1500.,300,-300.,300.  );
 
   h2rpot2 = new TH2F("h2rpot2" ,"h2rpot2", 100,  0.0  , 1500.,100,-300.,300.  );
   h2offmom2 = new TH2F("h2offmom2" ,"h2offmom2", 100,  0.0, 1500., 100,-300.,300.  );
   h2offmom22 = new TH2F("h2offmom22" ,"h2offmom22", 100,  0.0, 1500., 100,-300.,300.  );

   h2negtrk1 = new TH2F("h2negtrk1" ,"h2negtrk1", 100,  -50.0  , 350.,100,-200.,200.  );
   h2B0trk1 = new TH2F("h2B0trk1" ,"h2B0trk1", 100,  -50.0, 350., 100,-200.,200.  );
  
   h2negtrk2 = new TH2F("h2negtrk2" ,"h2negtrk2", 100,  -50.0  , 350.,100,-200.,200.  );
   h2B0trk2 = new TH2F("h2B0trk2" ,"h2B0trk2", 100,  -50.0, 350.,100,-200.,200.  );
 
   h2ci_GEM = new TH2F("h2ci_GEM" ,"h2ci_GEM", 100,  -1400.0  , 1400.,100,-1400.,1400.  );
   h2ci_GEMp = new TH2F("h2ci_GEMp" ,"h2ci_GEMp", 100,  -1400.0  , 1400.,100,-1400.,1400.  );
   h2cb_CTD = new TH2F("h2cb_CTD" ,"h2cb_CTD", 100,  -2000.0  , 2000.,100,-2000.,2000.  );
   h2ce_GEM = new TH2F("h2ce_GEM" ,"h2ce_GEM", 100,  -1400.0  , 1400.,100,-1400.,1400.  );
   h2ci_HCAL= new TH2F("h2ci_HCAL" ,"h2ci_HCAL", 100,  -100.0 , 200.,100,-200.,200.  );
   h2corrz = new TH2F("h2corrz" ,"h2corrz", 100,  -300.0  , 30000.,100,-300.,30000. );

   //-----------------------------------------------------
   hPhi0_mass  = new TH1F("hPhi0_mass" ,"#phi^{0} mass " , 300, 0.95 , 1.1  );
   hD0_mass  = new TH1F("hD0_mass" ,"D0 mass " , 100, 1.4   , 2.4  );
   hD0_pt    = new TH1F("hD0_pt" ,"D0 pt "     , 100, 0.0   , 3.0  );

   hDsbs_mass  = new TH1F("hDsbs_mass" ,"Ds mass", 100, 1.9 , 2.1  );  hDsbs_mass->GetXaxis()->SetTitle("m_{KK#pi} [GeV/c^{2}]");
   hDst_dm   = new TH1F("hDst_dm"  ,"D*-D0 "   , 100, 0.139 , 0.17 );
   hDst_dm2  = new TH1F("hDst_dm2"  ,"D*-D0 rec"   , 100, 0.139 , 0.17 );
   hDst_mass  = new TH1F("hDst_mass" ,"D* std cut", 100, 1.4 , 2.4  );  hDst_mass->GetXaxis()->SetTitle("m_{K#pi#pi} [GeV/c^{2}]");
   hDst_mass1 = new TH1F("hDst_mass1","D* hard cut", 100, 1.4 , 2.4  );  hDst_mass1->GetXaxis()->SetTitle("m_{K#pi#pi} [GeV/c^{2}]");
   hDst_mass2 = new TH1F("hDst_mass2","D* soft cut ", 100, 1.4 , 2.4  );  hDst_mass2->GetXaxis()->SetTitle("m_{K#pi#pi} [GeV/c^{2}]");
   hDst_pt   = new TH1F("hDst_pt","D* pt "     , 100, 0.    , 6.0  );

   hL0_mass = new TH1F("hL0_mass" ,"hL0_mass" , 100,  0.0   , 2.0  );
   hL0_perp = new TH1F("hL0_perp" ,"hL0_perp" , 100,  0.0   , 2.0  );
   hL0_perp2 = new TH1F("hL0_perp2" ,"hL0_perp2" , 100,  0.0   , 2.0  );

   hL0_perp_sm = new TH1F("hL0_perp_sm" ,"#Lambda^{0}_{pT} ; pT, [GeV/c]" , 50,  0.0   , 1.0  );
   hL0_perp_sm->SetFillColor(kYellow);
   hL0_perp2_sm = new TH1F("hL0_perp2_sm" ,"t (#Lambda^{0}) ; t, [Gev^{2}]" , 100,  0.0   , 0.3  );
   hL0_perp2_sm->SetFillColor(kYellow);
   hL0_mass_sm = new TH1F("hL0_mass_sm" ,"Invariant mass #Lambda^{0} ; M ( p,#pi^{-} )  [GeV/c^{2}]" , 100,  0.6   , 1.8  );
   hL0_mass_sm->SetFillColor(kYellow);

   hXLp= new TH1D("hXLp","proton x_{L} ; x_{L}",100,0.,1.0);
   hXLp->SetFillColor(kYellow);
   hXLpi = new TH1D("hXLpi"," #pi^{-} x_{L} ; x_{L}",100,0.,1.0);
   hXLpi->SetFillColor(kYellow);
   hTHETAp = new TH1D("hTHETAp","proton theta ; #Theta, [mrad]",100,0.,20.);
   hTHETAp->SetFillColor(kYellow);
   hTHETApi = new TH1D("hTHETApi","#pi^{-} theta ; #Theta, [mrad]",100,0.,20.);
   hTHETApi->SetFillColor(kYellow);

   random = new TRandom3();

   Notify();
}

Bool_t Lambda::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void Lambda::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t Lambda::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
//==========================================================================

//==================================================================
void Lambda::Count(const char *tit) {
  hcount->Fill(tit,1);
}
void Lambda::Count(const char *tit, double cut1) {
  char clab[20];
  sprintf(clab,"%s_%.1f",tit,cut1);    
  hcount->Fill(clab,1);
}
void Lambda::Count(const char *tit, double cut1, double cut2) {
  char clab[20];
  sprintf(clab,"%s_%.1f_%.1f",tit,cut1,cut2);    
  hcount->Fill(clab,1);
}
//==================================================================
void Lambda::PFillx(int i,int ih) {
  hPX[ih]->Fill(vPion[i].Px());
  hPY[ih]->Fill(vPion[i].Py());
  hPZ[ih]->Fill(vPion[i].Pz());
  hPT[ih]->Fill(vPion[i].Perp());
  hPTOT[ih]->Fill(vPion[i].Vect().Mag());
  hPHI[ih]->Fill(vPion[i].Phi());
  hTHETA[ih]->Fill(vPion[i].Theta()*1000.);
  //heta->Fill();
  hCHARGE[ih]->Fill(vCharge.at(i)+0.);
}
//==================================================================
void Lambda::PFill(int i) {
  hpx->Fill(vPion[i].Px());
  hpy->Fill(vPion[i].Py());
  hpz->Fill(vPion[i].Pz());
  hpt->Fill(vPion[i].Perp());
  hptot->Fill(vPion[i].Vect().Mag());
  hphi->Fill(vPion[i].Phi());
  htheta->Fill(vPion[i].Theta());
  //heta->Fill();
  hcharge->Fill(vCharge.at(i)+0.);
}
//==================================================================
void Lambda::PFill1(int i) {
  hpx1->Fill(vPion[i].Px());
  hpy1->Fill(vPion[i].Py());
  hpz1->Fill(vPion[i].Pz());
  hpt1->Fill(vPion[i].Perp());
  hptot1->Fill(vPion[i].Vect().Mag());
  hphi1->Fill(vPion[i].Phi());
  htheta1->Fill(vPion[i].Theta()*1000);
  h2vtxptot1->Fill(vVtx[i].Mag()/1000.,vPion[i].Vect().Mag());
 h2vtxtheta1->Fill(vVtx[i].Mag()/1000.,vPion[i].Theta()*1000);
  //heta->Fill();
  hcharge1->Fill(vCharge.at(i)+0.);
}
//==================================================================
void Lambda::PFill2(int i) {
  hpx2->Fill(vPion[i].Px());
  hpy2->Fill(vPion[i].Py());
  hpz2->Fill(vPion[i].Pz());
  hpt2->Fill(vPion[i].Perp());
  hptot2->Fill(vPion[i].Vect().Mag());
  hphi2->Fill(vPion[i].Phi());
  htheta2->Fill(vPion[i].Theta()*1000.);
  h2vtxptot2->Fill(vVtx[i].Mag()/1000.,vPion[i].Vect().Mag());
  h2vtxtheta2->Fill(vVtx[i].Mag()/1000.,vPion[i].Theta()*1000);
 //heta->Fill();
  hcharge2->Fill(vCharge.at(i)+0.);
}

//-------------------------------------------------------------------------------
//                           D*
//-------------------------------------------------------------------------------
//  .x nt_chain.C("DATA3");
//  chain.Process("genf.C+","dstmc");
//
//  tracks->Process("genf.C+","dstmc");
//  tracks->Process("genf.C+","dstrec");
//

#define BTEST(var,pos) ((var & (1 << pos)) == (1 << pos))

int Lambda::DSTARg4() {

  //================================================================================================
  double MassPI=0.139570, MassK=0.493677, MassP=0.93827, MassE=0.000511, MassMU=0.105658; 

  //double  MD0INF1=1.800,  MD0SUP1=1.900, DIFinf=0.140, DIFsup=0.150;
  double  MD0INF1=1.830,  MD0SUP1=1.890, DIFinf=0.143, DIFsup=0.148;
  //double  MD0INF1=1.840,  MD0SUP1=1.880, DIFinf=0.140, DIFsup=0.150;
  double  PtDScut=0.5,   PtD0cut=0.3,    PTCUT1=0.3,       PTCUT2=0.3,    ETAcut=2.75, EtaDScut=2.5;
  double  VTX12cut=10,  VTX12zcut=5,  VTX13cut=10,     VTX13zcut=5;
  double  EOPcut=0.8,  THETA0=0.033 ;  //-- Theta cut  2 grad :: 0.033  -- 20x20 cm in fcal
  //================================================================================================


  /*
  double PISL[4], PISP[4], PFSL[4];
  double X,Y,Q2;
  PFSL[0]=1; PFSL[1]=2; PFSL[2]=3; PFSL[3]=sqrt(4.);
  int rc = GEN_XYQ2(PFSL,PISL,PISP,X,Y,Q2);
  */
  //printf(" trkID->size()=%d \n",trkID->size());
  Count("EVT");
  int ncharged=0;
  for (int ip=0; ip<Ntracks; ip++) {
    if  (vCharge.at(ip)==0)  continue; 
    ncharged++;
    //printf("mass pi=%f charge=%d PDG=%d ParID=%d \n",vPion[ip].M(),vCharge.at(ip),vPDG.at(ip),vParID.at(ip));
  }
  hntrk->Fill(ncharged+0.);
  //---------------------------------------------------------------------------------------------------
  //                       I loop  K 
  //---------------------------------------------------------------------------------------------------
  for (int I=0; I<Ntracks; I++) {   Count("I-loop");   //    " --- Loop over K ---"; 

    if  (vCharge.at(I)==0) continue;  Count("I-charge"); 
    //if (vCharge.at(I)>0 )  continue; 
    //PFill(I); 

    //--------------------------------------------------------------------------------------------------
    //                       J loop  PI
    //--------------------------------------------------------------------------------------------------
    for (int J=0; J<Ntracks; J++) {    Count("J-loop"); //  " --- Loop over PI   ---";

      if ( (I==J) || ( vKaon[I].Perp()<PTCUT2 && vPion[J].Perp()<PTCUT2 ) || ( vCharge.at(I)*vCharge.at(J)>=0) )  continue;   Count("J-charge"); 
      //PFill(J); 
      TLorentzVector D0 =  vKaon[I]+vPion[J];      hD0_mass->Fill(D0.M());
      double InvD0 = D0.M();                       
      double D0_pt = D0.Perp();
      if ( D0_pt<PtD0cut)     continue;     Count("D0_pt",PtD0cut);      //--- D0 Pt cut ---

      TVector3 dvtx = vVtx[I]-vVtx[J];  hdv1->Fill(dvtx.Mag());
      if (dvtx.Mag()>VTX12cut) continue;  Count("D0_vtx",VTX12cut);      //--- D0 vtx cut ---

      //------   select  1.79<D0<1.93   -------

      if (MD0INF1>InvD0  ||  InvD0>MD0SUP1)    continue;     Count("D0_mass",MD0INF1,MD0SUP1); 

      //---------------------------------------------------------------------------------------------------
      //                       K loop PI-slow
      //---------------------------------------------------------------------------------------------------
      for (int K=0; K<Ntracks; K++) {     Count("K-loop"); // ---  loop over PI slow  ----  
        if (K==I || K==J || ( vCharge.at(I)*vCharge.at(K)>=0)) continue;    Count("K-charge");   //-- request good charge --
        
        //PFill(K); 

        TVector3 dvtx2 = vVtx[I]-vVtx[K];   hdv2->Fill(dvtx2.Mag());
        if (dvtx2.Mag()>VTX13cut) continue;  Count("DS_vtx",VTX13cut);      //--- D0 vtx cut ---

        TLorentzVector Dstar = D0 + vPion[K];
        double InvDS = Dstar.M();                          hDst_mass->Fill(InvDS);
        double DIFF = InvDS-InvD0; // if (DIFF<0.3) printf("DIFF=%f %d,%d,%d \n",DIFF,I,J,K);             

        hDst_dm->Fill(DIFF);    h2->Fill(DIFF,D0_pt);

        if ( DIFinf>DIFF || DIFF>DIFsup )             continue;   Count("dM",DIFinf);       //---- select  0.14<DIFF<0.15
        if (Dstar.Perp()<PtDScut)                     continue;   Count("DS_Pt",PtDScut);   //----  D* Pt cut
                
        hD0_pt->Fill(D0_pt);


        //==================================   C H E C K        ======================================================
	/*
        if ( DIFinf>0.1450 || DIFF>0.1458 )             continue;   Count("MC_pk",PtDScut); 
        PFill(K); 
        //-----  search refitted tracks ---
        TLorentzVector Ka,Pi,Pis,D0t,DSt,DMt; int ic=0,jc=0,kc=0;
        for (int ip=0; ip<track_size; ip++) {
          if (vTrkID[I] == trkID->at(ip)) { Ka.SetXYZM(pxvtx->at(ip),pyvtx->at(ip),pzvtx->at(ip),MassK); ic++; }
          if (vTrkID[J] == trkID->at(ip)) { Pi.SetXYZM(pxvtx->at(ip),pyvtx->at(ip),pzvtx->at(ip),MassPI); jc++; }
          if (vTrkID[K] == trkID->at(ip)) { Pis.SetXYZM(pxvtx->at(ip),pyvtx->at(ip),pzvtx->at(ip),MassPI); kc++; }
        }
        if (ic==1 && jc==1 && kc==1 ) {
          D0t=Ka+Pi; DSt=Ka+Pi+Pis;  DMt=DSt-D0t;
          printf("mD0=%f (%f) mDS=%f (%f) DM=%f (%f) \n",D0t.M(),InvD0,  DSt.M(),InvDS,  DIFF,DSt.M()-D0t.M() );
          printf("=> pKa=%f (%f) pPi=%f (%f) pPis=%f (%f %f) \n", Ka.Vect().Mag(), vKaon[I].Vect().Mag() ,Pi.Vect().Mag(),vPion[J].Vect().Mag(), Pis.Vect().Mag(), vPion[K].Vect().Mag());
          hDst_dm2->Fill(DSt.M()-D0t.M());
        } else {
          printf("Error tracks finding: I=%d J=%d K=%d \n",ic,jc,kc);
        }
	*/
        //============================================================================================================

      }
    }
  }

 return 0;
}

  //-----------------------------------------------------------------------------------------------------------
int Lambda::LambdaP() {

  //================================================================================================
  double MassPI=0.139570, MassK=0.493677, MassP=0.93827, MassE=0.000511, MassMU=0.105658; 
  //================================================================================================


  /*
  double PISL[4], PISP[4], PFSL[4];
  double X,Y,Q2;
  PFSL[0]=1; PFSL[1]=2; PFSL[2]=3; PFSL[3]=sqrt(4.);
  int rc = GEN_XYQ2(PFSL,PISL,PISP,X,Y,Q2);
  */
  //printf(" trkID->size()=%d \n",trkID->size());
  Count("EVT");
  int ncharged=0;
  for (int ip=0; ip<Ntracks; ip++) {
    if  (vCharge.at(ip)==0)  continue; 
    ncharged++;
    //printf("mass pi=%f charge=%d PDG=%d ParID=%d \n",vPion[ip].M(),vCharge.at(ip),vPDG.at(ip),vParID.at(ip));
  }
  hntrk->Fill(ncharged+0.);
  int JFLAG=1;
  //---------------------------------------------------------------------------------------------------
  //                       I loop  Proton
  //---------------------------------------------------------------------------------------------------
  for (int I=0; I<Ntracks; I++) {  // Count("I-loop");   //    " --- Loop over Protons ---"; 

    
    if (vCharge.at(I)==0)  continue;  Count("I-charge"); 
    if (vCharge.at(I)<0 )  continue;   // proton
    if (vPDG.at(I)!=2212) continue;
    //printf(" proton:: ch=%d pdg=%d \n",vCharge.at(I),vPDG.at(I));

    //printf("Proton volume = %d \n",vVol[I]);

    
    if ( vVol.at(I) == 1  ) PFillx(I,0);   //-- RPOT
    if ( vVol.at(I) == 2  ) PFillx(I,1);   //-- OFFM
    if ( vVol.at(I) == 4  ) PFillx(I,2);   //-- B0


    

    PFill1(I);
    //--------------------------------------------------------------------------------------------------
    //                       J loop  PI-
    //--------------------------------------------------------------------------------------------------
    for (int J=0; J<Ntracks; J++) {   //  Count("J-loop"); //  " --- Loop over Pions   ---";
      
      //if ( vCharge.at(I)*vCharge.at(J)>=0 )  continue;   Count("J-charge"); 
      //printf(" pion:: ch=%d pdg=%d \n",vCharge.at(J),vPDG.at(J));
      if (vPDG.at(J)!=-211) continue;

      if ( vVol.at(J) == 1  ) PFillx(J,3);   //-- RPOT
      if ( vVol.at(J) == 2  ) PFillx(J,4);   //-- OFFM
      if ( vVol.at(J) == 4  ) PFillx(J,5);   //-- B0
     
      PFill2(J); 

      TLorentzVector L0 =  vProton[I]+vPion[J];
      double InvL0 = L0.M();                hL0_mass->Fill(InvL0);        
      double L0_pt = L0.Perp();             hL0_perp->Fill(L0_pt); 
      //printf("Pion volume = %d \n",vVol.at(J));
      h2corrz->Fill(vVtx.at(I).Z(),vVtx.at(J).Z());

      // -- smearing
      double res=0.060; /// GeV
      double px1=vProton[I].X()+random->Gaus(0.,res);    double py1=vProton[I].Y()/*+random->Gaus(0.,res)*/;    double pz1=vProton[I].Z();    TLorentzVector vp;   vp.SetXYZM(px1,py1,pz1,MassP); 
      double px2=vPion[J].X()+random->Gaus(0.,res);      double py2=vPion[J].Y()/*+random->Gaus(0.,res)*/;      double pz2=vPion[J].Z();      TLorentzVector vpi;  vpi.SetXYZM(px2,py2,pz2,MassPI); 
      TLorentzVector L0sm =  vp+vpi;
      double InvL0sm = L0sm.M();                hL0_mass_sm->Fill(InvL0sm);        
      double L0_ptsm = L0sm.Perp();             hL0_perp_sm->Fill(L0_ptsm); hL0_perp2_sm->Fill(L0_ptsm*L0_ptsm); 



      hXLp->Fill(vProton[I].Vect().Mag()/EPbeam);
      hXLpi->Fill(vPion[J].Vect().Mag()/EPbeam);
      hTHETAp->Fill(vProton[I].Theta()*1000.);
      hTHETApi->Fill(vPion[J].Theta()*1000.);

    }        
    
 }
  
  
 return 0;
}


  //-----------------------------------------------------------------------------------------------------------
int Lambda::LambdaN() {

  //================================================================================================
  double MassPI=0.139570, MassK=0.493677, MassP=0.93827, MassE=0.000511, MassMU=0.105658; 
  //================================================================================================


  /*
  double PISL[4], PISP[4], PFSL[4];
  double X,Y,Q2;
  PFSL[0]=1; PFSL[1]=2; PFSL[2]=3; PFSL[3]=sqrt(4.);
  int rc = GEN_XYQ2(PFSL,PISL,PISP,X,Y,Q2);
  */
  //printf(" trkID->size()=%d \n",trkID->size());
  Count("EVT");
  int ncharged=0;
  for (int ip=0; ip<Ntracks; ip++) {
    if  (vCharge.at(ip)!=0)  continue; 
    ncharged++;
    //printf("mass pi=%f charge=%d PDG=%d ParID=%d \n",vPion[ip].M(),vCharge.at(ip),vPDG.at(ip),vParID.at(ip));
  }
  hntrk->Fill(ncharged+0.);
  //---------------------------------------------------------------------------------------------------
  //                       I loop  Neutron
  //---------------------------------------------------------------------------------------------------
  for (int I=0; I<Ntracks; I++) {   Count("I-loop");   //    " --- Loop over neutron ---"; 

    if  (vCharge.at(I)!=0) continue;  Count("I-charge"); 
    //PFill(I); 

    //--------------------------------------------------------------------------------------------------
    //                       J loop  gamma 1
    //--------------------------------------------------------------------------------------------------
    for (int J=0; J<Ntracks; J++) {    Count("J-loop"); //  " --- Loop over PI   ---";

      if ( I==J || vCharge.at(J)!=0 )  continue;   Count("J-charge"); 
      //PFill(J); 

     //---------------------------------------------------------------------------------------------------
      //                       K loop gamma 2
      //---------------------------------------------------------------------------------------------------
      for (int K=0; K<Ntracks; K++) {     Count("K-loop"); // ---  loop over PI slow  ----  
        if (K==I || K==J || vCharge.at(K)!=0 ) continue;    Count("K-charge");   //-- request good charge --
        
        //PFill(K); 

	TVector3 dvtx2 = vVtx[J]-vVtx[K];   hdv2->Fill(dvtx2.Mag());
        if (dvtx2.Mag()>0.1) continue;  Count("DS_vtx",0.1);      //--- pi0 vtx cut ---

	TLorentzVector PI0 = vGamma[J]+vGamma[K];

	TLorentzVector L0 =  vNeutron[I]+PI0;     
	double InvL0 = L0.M();                hL0_mass->Fill(InvL0);        
	double L0_pt = L0.Perp();

      }
    }
  }


 return 0;
}



#endif // #ifdef Lambda_cxx
