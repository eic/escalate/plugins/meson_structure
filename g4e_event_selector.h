//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Mon Aug 31 11:49:16 2020 by ROOT version 6.20/04
// from TTree events/Flattened root tree with event data
// found on file: OUTPUTS/g4e_k_lambda_5on41_all_l3.root
//////////////////////////////////////////////////////////

#ifndef g4e_event_selector_h
#define g4e_event_selector_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TSelector.h>
#include <TTreeReader.h>
#include <TTreeReaderValue.h>
#include <TTreeReaderArray.h>

// Headers needed by this particular selector
#include <vector>

#include <string>



class g4e_event_selector : public TSelector {
public :
   TTreeReader     fReader;  //!the tree reader
   TTree          *fChain = 0;   //!pointer to the analyzed TTree or TChain

   // Readers to access the data (delete the ones you do not need).
   TTreeReaderValue<ULong64_t> event_id = {fReader, "event_id"};
   TTreeReaderValue<Double_t> evt_true_q2 = {fReader, "evt_true_q2"};
   TTreeReaderValue<Double_t> evt_true_x = {fReader, "evt_true_x"};
   TTreeReaderValue<Double_t> evt_true_y = {fReader, "evt_true_y"};
   TTreeReaderValue<Double_t> evt_true_w2 = {fReader, "evt_true_w2"};
   TTreeReaderValue<Double_t> evt_true_nu = {fReader, "evt_true_nu"};
   TTreeReaderValue<Double_t> evt_true_t_hat = {fReader, "evt_true_t_hat"};
   TTreeReaderValue<Char_t> evt_has_dis_info = {fReader, "evt_has_dis_info"};
   TTreeReaderValue<Double_t> evt_weight = {fReader, "evt_weight"};
   TTreeReaderValue<ULong64_t> hit_count = {fReader, "hit_count"};
   TTreeReaderArray<unsigned long> hit_id = {fReader, "hit_id"};
   TTreeReaderArray<unsigned long> hit_trk_id = {fReader, "hit_trk_id"};
   TTreeReaderArray<unsigned long> hit_ptr_id = {fReader, "hit_ptr_id"};
   TTreeReaderArray<unsigned long> hit_parent_trk_id = {fReader, "hit_parent_trk_id"};
   TTreeReaderArray<string> hit_vol_name = {fReader, "hit_vol_name"};
   TTreeReaderArray<double> hit_x = {fReader, "hit_x"};
   TTreeReaderArray<double> hit_y = {fReader, "hit_y"};
   TTreeReaderArray<double> hit_z = {fReader, "hit_z"};
   TTreeReaderArray<unsigned long> hit_i_rep = {fReader, "hit_i_rep"};
   TTreeReaderArray<unsigned long> hit_j_rep = {fReader, "hit_j_rep"};
   TTreeReaderArray<double> hit_e_loss = {fReader, "hit_e_loss"};
   TTreeReaderValue<ULong64_t> trk_count = {fReader, "trk_count"};
   TTreeReaderArray<unsigned long> trk_id = {fReader, "trk_id"};
   TTreeReaderArray<long> trk_pdg = {fReader, "trk_pdg"};
   TTreeReaderArray<unsigned long> trk_parent_id = {fReader, "trk_parent_id"};
   TTreeReaderArray<long> trk_create_proc = {fReader, "trk_create_proc"};
   TTreeReaderArray<unsigned long> trk_level = {fReader, "trk_level"};
   TTreeReaderArray<double> trk_vtx_x = {fReader, "trk_vtx_x"};
   TTreeReaderArray<double> trk_vtx_y = {fReader, "trk_vtx_y"};
   TTreeReaderArray<double> trk_vtx_z = {fReader, "trk_vtx_z"};
   TTreeReaderArray<double> trk_vtx_dir_x = {fReader, "trk_vtx_dir_x"};
   TTreeReaderArray<double> trk_vtx_dir_y = {fReader, "trk_vtx_dir_y"};
   TTreeReaderArray<double> trk_vtx_dir_z = {fReader, "trk_vtx_dir_z"};
   TTreeReaderArray<double> trk_mom = {fReader, "trk_mom"};
   TTreeReaderValue<ULong64_t> gen_prt_count = {fReader, "gen_prt_count"};
   TTreeReaderArray<unsigned long> gen_prt_id = {fReader, "gen_prt_id"};
   TTreeReaderArray<unsigned long> gen_prt_vtx_id = {fReader, "gen_prt_vtx_id"};
   TTreeReaderArray<unsigned long> gen_prt_pdg = {fReader, "gen_prt_pdg"};
   TTreeReaderArray<unsigned long> gen_prt_trk_id = {fReader, "gen_prt_trk_id"};
   TTreeReaderArray<double> gen_prt_charge = {fReader, "gen_prt_charge"};
   TTreeReaderArray<double> gen_prt_dir_x = {fReader, "gen_prt_dir_x"};
   TTreeReaderArray<double> gen_prt_dir_y = {fReader, "gen_prt_dir_y"};
   TTreeReaderArray<double> gen_prt_dir_z = {fReader, "gen_prt_dir_z"};
   TTreeReaderArray<double> gen_prt_tot_mom = {fReader, "gen_prt_tot_mom"};
   TTreeReaderArray<double> gen_prt_tot_e = {fReader, "gen_prt_tot_e"};
   TTreeReaderArray<double> gen_prt_time = {fReader, "gen_prt_time"};
   TTreeReaderArray<double> gen_prt_polariz_x = {fReader, "gen_prt_polariz_x"};
   TTreeReaderArray<double> gen_prt_polariz_y = {fReader, "gen_prt_polariz_y"};
   TTreeReaderArray<double> gen_prt_polariz_z = {fReader, "gen_prt_polariz_z"};
   TTreeReaderValue<ULong64_t> gen_vtx_count = {fReader, "gen_vtx_count"};
   TTreeReaderArray<unsigned long> gen_vtx_id = {fReader, "gen_vtx_id"};
   TTreeReaderArray<unsigned long> gen_vtx_part_count = {fReader, "gen_vtx_part_count"};
   TTreeReaderArray<double> gen_vtx_x = {fReader, "gen_vtx_x"};
   TTreeReaderArray<double> gen_vtx_y = {fReader, "gen_vtx_y"};
   TTreeReaderArray<double> gen_vtx_z = {fReader, "gen_vtx_z"};
   TTreeReaderArray<double> gen_vtx_time = {fReader, "gen_vtx_time"};
   TTreeReaderArray<double> gen_vtx_weight = {fReader, "gen_vtx_weight"};
   TTreeReaderArray<string> ce_emcal_name = {fReader, "ce_emcal_name"};
   TTreeReaderArray<double> ce_emcal_Etot_dep = {fReader, "ce_emcal_Etot_dep"};
   TTreeReaderArray<int> ce_emcal_Npe = {fReader, "ce_emcal_Npe"};
   TTreeReaderArray<double> ce_emcal_ADC = {fReader, "ce_emcal_ADC"};
   TTreeReaderArray<double> ce_emcal_TDC = {fReader, "ce_emcal_TDC"};
   TTreeReaderArray<double> ce_emcal_xcrs = {fReader, "ce_emcal_xcrs"};
   TTreeReaderArray<double> ce_emcal_ycrs = {fReader, "ce_emcal_ycrs"};
   TTreeReaderArray<double> ce_emcal_zcrs = {fReader, "ce_emcal_zcrs"};


   g4e_event_selector(TTree * /*tree*/ =0) { }
   virtual ~g4e_event_selector() { }
   virtual Int_t   Version() const { return 2; }
   virtual void    Begin(TTree *tree);
   virtual void    SlaveBegin(TTree *tree);
   virtual void    Init(TTree *tree);
   virtual Bool_t  Notify();
   virtual Bool_t  Process(Long64_t entry);
   virtual Int_t   GetEntry(Long64_t entry, Int_t getall = 0) { return fChain ? fChain->GetTree()->GetEntry(entry, getall) : 0; }
   virtual void    SetOption(const char *option) { fOption = option; }
   virtual void    SetObject(TObject *obj) { fObject = obj; }
   virtual void    SetInputList(TList *input) { fInput = input; }
   virtual TList  *GetOutputList() const { return fOutput; }
   virtual void    SlaveTerminate();
   virtual void    Terminate();

   ClassDef(g4e_event_selector,0);

};

#endif

#ifdef g4e_event_selector_cxx
void g4e_event_selector::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the reader is initialized.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   fReader.SetTree(tree);
}

Bool_t g4e_event_selector::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}


#endif // #ifdef g4e_event_selector_cxx
