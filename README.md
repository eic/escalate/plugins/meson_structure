# meson_structure

Meson structure analysis

```bash
ejana -Pplugins=g4e_reader,mstruct_lambda  -Pnthreads=1 -Pmstruct_lambda:verbose=1 -Pnevents=1000 -Poutput=OUTPUTS/lambda_analysis.root  OUTPUTS/g4e_k_lambda_5on41_all_l3.root  -Pjana:debug_plugin_loading=1 
```

Runnin ejana with meson_lambda plugin from a command line:

```
ejana
-Pplugins=g4e_reader,mstruct_lambda
-Pnthreads=1
-Pmstruct_lambda:verbose=1
-Pmstruct_lambda:e_beam_energy=18
-Pmstruct_lambda:ion_beam_energy=275
-Pnevents=500000
-Poutput=/home/romanov/eic/meson_structure/OUTPUTS/lambda_ana_18on275_all_l3.root
/home/romanov/eic/meson_structure/OUTPUTS/g4e_k_lambda_18on275_all_l3.root
-Pjana:debug_plugin_loading=1
```

CMake environment for Dmitry PC
```bash
JANA_PLUGIN_PATH=-DEJANA_HOME=/home/romanov/eic/spack/opt/spack/linux-ubuntu18.04-zen/gcc-7.5.0/ejana-1.2.3-7guymifhdqisskrtjdlnn5t4cnv57ebs/plugins
```


CMake options for Dmitry PC
```bash
-DCMAKE_PREFIX_PATH=/home/romanov/eic/meson_structure/mstruct_lambda
-DCMAKE_CXX_STANDARD=17
-DEJANA_HOME=/home/romanov/eic/spack/opt/spack/linux-ubuntu18.04-zen/gcc-7.5.0/ejana-1.2.3-7guymifhdqisskrtjdlnn5t4cnv57ebs
-DJANA_HOME=/home/romanov/eic/spack/opt/spack/linux-ubuntu18.04-zen/gcc-7.5.0/jana2-2.0.3-g2xebrpoz6kmlxrcs6ukmhg3j5bwdmow
-DROOT_DIR=/home/romanov/eic/spack/opt/spack/linux-ubuntu18.04-zen/gcc-7.5.0/root-6.20.04-75hx6iae6rwj7a2hddiirhzjgfkgk47g
-DCMAKE_PREFIX_PATH=/home/romanov/eic/spack/opt/spack/linux-ubuntu18.04-zen/gcc-7.5.0/root-6.20.04-75hx6iae6rwj7a2hddiirhzjgfkgk47g/
```

