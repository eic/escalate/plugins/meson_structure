#include "TLorentzVector.h"

#include <fmt/core.h>

#include <JANA/JEvent.h>

#include <dis/functions.h>
#include <MinimalistModel/McFluxHit.h>
#include <MinimalistModel/McGeneratedParticle.h>
#include <MinimalistModel/McTrack.h>

#include "LambdaProcessor.h"

#include <TClingRuntime.h>
#include <ejana/MainOutputRootFile.h>
#include <Math/GenVector/RotationY.h>


// TODO repalce by some 'conventional' PDG info provider. Like TDatabasePDG
//==================================================================
Double_t MassPICharged = 0.139570,
        MassPi0 = 0.1349766,
        MassK = 0.493677,
        MassProton = 0.93827,
        MassNeutron = 0.939565,
        MassE = 0.000511,
        MassMU = 0.105658;
//==================================================================

double const pi_to_deg = 180.0/3.14159265359;
double const deg_to_pi = 3.14159265359/180.0;

using namespace std;
using namespace fmt;


void LambdaProcessor::Init() {
    ///  Called once at program start.
    print("MesonStructureLambdaProcessor::Init()\n");

    // initialize class that holds histograms and other root objects
    // 'services' is a service locator, we ask it for TFile
    auto app = this->GetApplication();
    auto file = app->GetService<ej::MainOutputRootFile>();
    //auto file = new TFile("jopa.root", "RECREATE"); //app->GetService<ej::MainOutputRootFile>();
    root_out.init(file.get());


    // Ask service locator for parameter manager. We want to get this plugin parameters.


    // Verbosity level. 0 = show nothing. 1 - show some. 2 - mad printer
    // SetDefaultParameter actually sets the parameter value from arguments if it is specified
    verbose = 0;
    GetApplication()->SetDefaultParameter("mstruct_lambda:verbose", verbose,
                            "Plugin output level. 0-almost nothing, 1-some, 2-everything");

    // ion beam energy
    GetApplication()->SetDefaultParameter("mstruct_lambda:ion_beam_energy", ion_beam_energy,
                                          "ion beam energy");

    // electron beam energy
    GetApplication()->SetDefaultParameter("mstruct_lambda:e_beam_energy", e_beam_energy,
                                          "electron beam energy");
    if (verbose) {
        print("Parameters:\n");
        print("  mstruct_lambda:verbose:         {0}\n", verbose);
        print("  mstruct_lambda:ion_beam_energy: {0}\n", ion_beam_energy);
        print("  mstruct_lambda:e_beam_energy:   {0}\n", e_beam_energy);
    }
}

/// This function processes everything on event by event basis
void LambdaProcessor::Process(const std::shared_ptr<const JEvent> &event) {
    ///< Called every event.
    using namespace fmt;
    using namespace std;
    using namespace minimodel;

    // std::lock_guard<std::recursive_mutex> locker(root_out.lock);
    _stat.events_count++;

    // >oO debug printing
    if ( (event->GetEventNumber() % 1000 == 0 && verbose > 1) || verbose >= 3) {
        print("\n--------- EVENT {} --------- \n", event->GetEventNumber());
    }

    // Get all tracks
    auto tracks = event->Get<minimodel::McTrack>();

    // Get all hits
    auto hits = event->Get<minimodel::McFluxHit>();

    // We will push the selected tracks to these arrays
    std::vector<const minimodel::McTrack*> protons;
    std::vector<const minimodel::McTrack*> neutrons;
    std::vector<const minimodel::McTrack*> pi_zeros;
    std::vector<const minimodel::McTrack*> pimins;
    std::vector<const minimodel::McTrack*> gammas;

    // We also has to check that a track was in ion_emcal, zdc or barrel_emcal, to do so we will use this arrays
    std::unordered_set<uint64_t> track_ids_in_ion_emcal;
    std::unordered_set<uint64_t> track_ids_in_zdc;
    std::unordered_set<uint64_t> track_ids_in_barrel_emcal;

    // ids of tracks for gamma, neutron and lambda
    std::unordered_set<uint64_t> gammas_trk_ids;
    std::unordered_set<uint64_t> neutron_trk_ids;
    std::unordered_set<uint64_t> lambda_trk_ids;

    // loop over tracks
    for(auto track: tracks) {

        // Un-decayed lambda (that happens!)
        if(track->pdg == 3122) {
            // It is not decayed lambda
            lambda_trk_ids.insert(track->id);
            continue;
        }

        // Proton processing
        if(track->pdg == 2212) {

            // Fill some histograms and statistics
            _stat.proton++;
            root_out.h1_lam_p_decay_z->Fill(track->vtx_z/1000.);
            root_out.h1_lam_decay_z->Fill(track->vtx_z/1000.);

            // Add stats for lambdas decayed in 5m, 5-30m, 30+m
            UpdateDecayVertexStats(track);

            // Add proton to protons collection to analyse further
            protons.push_back(track);
        }       // protons  only

        // Neutron processing
        if(track->pdg == 2112) {

            // Update plots and stats
            _stat.neutron++;
            root_out.h1_lam_n_decay_z->Fill(track->vtx_z/1000.);
            root_out.h1_lam_decay_z->Fill(track->vtx_z/1000.);

            // Add stats for lambdas decayed in 5m, 5-30m, 30+m
            UpdateDecayVertexStats(track);

            // Add its id to collections
            neutron_trk_ids.insert(track->id);
            neutrons.push_back(track);
        }

        // Pi0 processing
        if(std::abs(track->pdg) == 111) {
            _stat.pi_zero++;
            pi_zeros.push_back(track);
        }

        // Pi- processing
        if(track->pdg == -211 ) {
            _stat.pi_minus++;
            pimins.push_back(track);
        }

        // Gamma processing
        if(track->pdg == 22) {

            _stat.all_gamma++;

            // There are just too many gammas, this will cut gammas to be from lambda mostly
            if(track->p > 0.5 && track->decay_level==2) {
                gammas_trk_ids.insert(track->id);
                _stat.pi0_gamma++;
                gammas.push_back(track);
            }
        }
    }

    //Now we h

    for(auto proton: protons) {
        for(auto pimin: pimins) {
            ProcessProtonPiMinus(event, proton, pimin);
        }
    }

    for(auto neutron: neutrons) {
        ROOT::Math::PxPyPzMVector n_vect(neutron->px, neutron->py, neutron->pz, MassNeutron);


        root_out.h1_n_etot->Fill(n_vect.E());
        root_out.h1_n_p->Fill(n_vect.P());
        root_out.h1_n_xl->Fill(n_vect.P() / ion_beam_energy);

        auto p = ROOT::Math::Cartesian3D(neutron->px, neutron->py, neutron->pz);
        ROOT::Math::RotationY r(-0.025);
        p = r(p);

        double pxy = std::sqrt(p.x()*p.x() + p.y()*p.y());
        double theta = std::atan(pxy/p.z());
        root_out.h1_n_theta->Fill(theta * 1000.);

        root_out.h1_n_pt->Fill(n_vect.Pt());

        for(auto pi_zero: pi_zeros) {
            ProcessNeutronPiZero(event, neutron, pi_zero);
        }

        for(size_t i=0; (i < gammas.size()-1) && gammas.size(); i++ ) {
            for(size_t j = i+1; j < gammas.size(); j++ ) {
                ProcessNeutron2Gammas(event, neutron, gammas[i], gammas[j]);
            }
        }
    }

    // Loop over hits
    for (auto hit: hits) {
        // it is gamma
        if(neutron_trk_ids.count(hit->track_id) && ej::StartsWith(hit->vol_name, "ffi_ZDC")) {
            root_out.h2_n_xy_hits_zdc->Fill(hit->x, hit->y);
            root_out.h1_n_z_hits_zdc->Fill(hit->z/1000.);
        }

        if(lambda_trk_ids.count(hit->track_id)) {
            root_out.h1_lam_z_hits_any->Fill(hit->z/1000.);
        }

        // we need only gamma hits further
        if(!gammas_trk_ids.count(hit->track_id)) continue;

        // Look at hits at hadron EMCAL
        if (ej::StartsWith(hit->vol_name, "ci_")) {
            track_ids_in_ion_emcal.insert(hit->track_id);   // Save the track id for further track processing

            // Fill some histograms
            root_out.h2_xy_hits_ioncap->Fill(hit->x, hit->y);
            root_out.h1_z_hits_any->Fill(hit->z);
        }

        // Look at hits at Barrel EMCAL
        if (ej::StartsWith(hit->vol_name, "cb_")) {
            track_ids_in_barrel_emcal.insert(hit->track_id);   // Save the track id for further track processing

            // Fill some histograms
            root_out.h2_xy_hits_barrel->Fill(hit->x, hit->y);
            root_out.h1_z_hits_barrel->Fill(hit->z);
            root_out.h1_z_hits_any->Fill(hit->z);
        }

        // Look at hits at electron EMCAL
        if (ej::StartsWith(hit->vol_name, "ffi_ZDC")) {
            track_ids_in_zdc.insert(hit->track_id);    // Save the track id for further track processing

            // Fill some histograms
            root_out.h2_xy_hits_zdc->Fill(hit->x, hit->y);
            root_out.h1_z_hits_any->Fill(hit->z);
        }
    }
}

void LambdaProcessor::UpdateDecayVertexStats(const minimodel::McTrack *track) {
    // Add stats for lambdas decayed in 5m, 5-30m, 30+m
    if(fabs(track->vtx_z) <= 5000) {
        _stat.decayed_5m_lambdas++;
    } else if(fabs(track->vtx_z) > 5000 && fabs(track->vtx_z) <= 30000) {
        _stat.decayed_5m_30m_lambdas++;
    } else {
        _stat.decayed_after_30m_lambdas++;
    }
}


void LambdaProcessor::Finish() {
    ///< Called after last event of last event source has been processed.
    print("MesonStructureLambdaProcessor::Finish(). Cleanup\n");
    print("  'events_count'        : {}, \n", _stat.events_count       );
    print("  'proton'              : {}, \n", _stat.proton);
    print("  'pi_minus'            : {}, \n", _stat.pi_minus);
    print("  'neutron'             : {}, \n", _stat.neutron);
    print("  'pi_zero'             : {}, \n", _stat.pi_zero);
    print("  'pi0_gamma'           : {}, \n", _stat.pi0_gamma);
    print("  'all_gamma'           : {}, \n", _stat.all_gamma);
    print("  'decayed_5m_lambdas'        : {}, \n", _stat.decayed_5m_lambdas);
    print("  'decayed_5m_30m_lambdas'    : {}, \n", _stat.decayed_5m_30m_lambdas);
    print("  'decayed_after_30m_lambdas' : {}, \n", _stat.decayed_after_30m_lambdas);
}

void LambdaProcessor::ProcessProtonPiMinus(const shared_ptr<const JEvent> &sharedPtr,
                                           const minimodel::McTrack *proton_trk,
                                           const minimodel::McTrack *pion_trk) {
    // α Arm = (p + L − p L )/(p L + p L )

    ROOT::Math::PxPyPzMVector pi(pion_trk->px, pion_trk->py, pion_trk->pz, MassPICharged);
    ROOT::Math::PxPyPzMVector prot(proton_trk->px, proton_trk->py, proton_trk->pz, MassProton);
    auto lam = prot + pi;

    root_out.h1_pim_theta->Fill(CalculateTheta(pi.px(), pi.py(), pi.pz()) * 1000.);
    root_out.h1_pim_etot->Fill(pi.E());
    root_out.h1_pim_p->Fill(pi.P());
    root_out.h1_pim_xl->Fill(pi.P()/ion_beam_energy);

    root_out.h1_prot_theta->Fill(CalculateTheta(prot.px(), prot.py(), prot.pz()) * 1000.);
    root_out.h1_prot_etot->Fill(prot.E());
    root_out.h1_prot_p->Fill(prot.P());
    root_out.h1_prot_pt->Fill(prot.Pt());
    root_out.h1_prot_xl->Fill(prot.P()/ion_beam_energy);

    double lam_p_angle = pi_to_deg * asin(lam.Vect().Unit().Cross(prot.Vect().Unit()).R());
    double lam_pim_angle = pi_to_deg * asin(lam.Vect().Unit().Cross(pi.Vect().Unit()).R());

    root_out.h1_lam_p_angle->Fill(lam_p_angle);
    root_out.h1_lam_pim_angle->Fill(lam_pim_angle);

    auto arm_a = (pi.Vect().mag2() - prot.Vect().mag2()) / lam.Vect().mag2();
    auto qt = (pi.Vect().Cross(prot.Vect()).R()) / lam.Vect().R();

    root_out.h2_lam_armenteros->Fill(arm_a, qt);
    root_out.h1_lam_m_ppi->Fill(lam.M());
}



void LambdaProcessor::ProcessNeutronPiZero(const shared_ptr<const JEvent> &sharedPtr,
                                           const minimodel::McTrack *neutron_trk,
                                           const minimodel::McTrack *pi0_trk) {
    // α Arm = (p + L − p L )/(p L + p L )

    ROOT::Math::PxPyPzMVector pi0(pi0_trk->px, pi0_trk->py, pi0_trk->pz, MassPi0);
    ROOT::Math::PxPyPzMVector neutron(neutron_trk->px, neutron_trk->py, neutron_trk->pz, MassNeutron);

    // Histogram pi0 parameter
    root_out.h1_pi0_etot->Fill(pi0.E());
    root_out.h1_pi0_p->Fill(pi0.P());
    root_out.h1_pi0_xl->Fill(pi0.P()/ion_beam_energy);
    root_out.h1_pi0_theta->Fill(pi0.Theta() * 1000.);

    // Construct lambda, fill the mass
    auto lam = neutron + pi0;
    root_out.h1_lam_npi0_mass->Fill(lam.M());

    // Angles
    double lam_n_angle = pi_to_deg * asin(lam.Vect().Unit().Cross(neutron.Vect().Unit()).R());
    double lam_pi0_angle = pi_to_deg * asin(lam.Vect().Unit().Cross(pi0.Vect().Unit()).R());
    root_out.h1_lam_n_angle->Fill(lam_n_angle);
    root_out.h1_lam_pi0_angle->Fill(lam_pi0_angle);
}

void LambdaProcessor::ProcessNeutron2Gammas(const std::shared_ptr<const JEvent> &sharedPtr,
                                           const minimodel::McTrack *neutron_trk,
                                           const minimodel::McTrack *gamma0,
                                           const minimodel::McTrack *gamma1
                                           ) {
    // α Arm = (p + L − p L )/(p L + p L )

    ROOT::Math::PxPyPzMVector g0(gamma0->px, gamma0->py, gamma0->pz, 0);
    ROOT::Math::PxPyPzMVector g1(gamma1->px, gamma1->py, gamma1->pz, 0);
    ROOT::Math::PxPyPzMVector neutron(neutron_trk->px, neutron_trk->py, neutron_trk->pz, MassNeutron);

    // Pi0 from 2 gammas mass:
    auto pi0 = g0 + g1;

    // Fill gammas params
    root_out.h1_gamma_etot->Fill(g0.E());
    root_out.h1_gamma_etot->Fill(g1.E());
    root_out.h1_gamma_p->Fill(g0.P());
    root_out.h1_gamma_p->Fill(g1.P());

    ROOT::Math::RotationY r(-0.025);
    auto p = r(ROOT::Math::Cartesian3D(gamma0->px, gamma0->py, gamma0->pz));
    double pxy = std::sqrt(p.x()*p.x() + p.y()*p.y());
    double theta = std::atan(pxy/p.z());
    root_out.h1_gamma_theta->Fill(theta * 1000.);

    root_out.h1_gamma_theta->Fill(CalculateTheta(gamma1->px, gamma1->py, gamma1->pz) * 1000.);

    // Histogram pi0 parameter
    root_out.h1_ggpi0_mass->Fill(pi0.M());
    root_out.h1_ggpi0_etot->Fill(pi0.E());
    root_out.h1_ggpi0_p->Fill(pi0.P());
    root_out.h1_ggpi0_xl->Fill(pi0.P()/ion_beam_energy);

    p = r(ROOT::Math::Cartesian3D(pi0.px(), pi0.py(), pi0.pz()));
    pxy = std::sqrt(p.x()*p.x() + p.y()*p.y());
    theta = std::atan(pxy/p.z());
    root_out.h1_ggpi0_theta->Fill(theta * 1000.);

    // Lambda mass
    auto lam = neutron + pi0;
    root_out.h1_lam_n2g_mass->Fill(lam.M());

    // Angles
    double lam_n_angle = pi_to_deg * asin(lam.Vect().Unit().Cross(neutron.Vect().Unit()).R());
    double lam_pi0_angle = pi_to_deg * asin(lam.Vect().Unit().Cross(pi0.Vect().Unit()).R());
    root_out.h1_lam_n_angle->Fill(lam_n_angle);
    root_out.h1_lam_pi0_angle->Fill(lam_pi0_angle);
}

double LambdaProcessor::CalculateTheta(double px, double py, double pz) {
    static const ROOT::Math::RotationY r(-0.025);
    auto p = r(ROOT::Math::Cartesian3D(px, py, pz));
    double pxy = sqrt(p.x()*p.x() + p.y()*p.y());
    double theta = atan(pxy/p.z());
    return theta;
}
