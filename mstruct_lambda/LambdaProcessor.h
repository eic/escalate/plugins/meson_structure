#ifndef MESON_STRUCTURE_LAMBDA_PROCESSOR_HEADER
#define MESON_STRUCTURE_LAMBDA_PROCESSOR_HEADER

#include <thread>
#include <atomic>
#include <mutex>

#include <JANA/JEventProcessor.h>
#include <JANA/JObject.h>

#include "Math/Point3D.h"
#include "Math/Vector4D.h"
#include "LambdaRootOutput.h"
#include <TLorentzVector.h>
#include <MinimalistModel/McTrack.h>

class JApplication;

class MesonStructureInputParticle:public JObject {
public:
    uint64_t id;  // unique particle id in the event
    ROOT::Math::PxPyPzMVector p;
//    ROOT::Math::XYZPoint vertex;
   // TVector3 p;
    TVector3 vertex;
    int pdg;
    double charge;
    int mother_id;
    int mother_pdg;
    int grand_mother_id;
    int grand_mother_pdg;
};

struct LambdaStatistics {
    uint64_t events_count = 0;
    uint64_t decayed_5m_lambdas = 0;
    uint64_t decayed_5m_30m_lambdas = 0;
    uint64_t decayed_after_30m_lambdas = 0;
    uint64_t proton = 0;
    uint64_t neutron = 0;
    uint64_t pi_zero = 0;
    uint64_t pi_minus = 0;
    uint64_t all_gamma = 0;
    uint64_t pi0_gamma = 0;
};


class LambdaProcessor : public JEventProcessor
{
  public:

    // Constructor just applies
    explicit LambdaProcessor(JApplication *app=nullptr):
            JEventProcessor(app)          
                {};

    // This is called once before the first call to the Process method
    // below. You may, for example, want to open an output file here.
    // Only one thread will call this.
    void Init() override;

    //----------------------------
    // Process
    //
    // This is called for every event. Multiple threads may call this
    // simultaneously. If you write something to an output file here
    // then make sure to protect it with a mutex or similar mechanism.
    // Minimize what is done while locked since that directly affects
    // the multi-threaded performance.
    void Process(const std::shared_ptr<const JEvent>& event) override;


    //----------------------------
    // Finish
    //
    // This is called once after all events have been processed. You may,
    // for example, want to close an output file here.
    // Only one thread will call this.
    void Finish() override;

private:
    LambdaRootOutput root_out;    
    int verbose;                // verbose output level
    int smearing;                // smearing level
    double e_beam_energy = 10;
    double ion_beam_energy = 100;
//    std::atomic<std::uint64_t> _undecayed_lam_count;
    LambdaStatistics _stat;

    void ProcessProtonPiMinus(const std::shared_ptr<const JEvent> &sharedPtr,
                              const minimodel::McTrack *pTrack,
                              const minimodel::McTrack *pTrack1);

    void ProcessNeutronPiZero(const std::shared_ptr<const JEvent> &sharedPtr, const minimodel::McTrack *neutron_trk,
                              const minimodel::McTrack *pi0_trk);

    void ProcessNeutron2Gammas(const std::shared_ptr<const JEvent> &sharedPtr,
                          const minimodel::McTrack *neutron_trk,
                          const minimodel::McTrack *gamma0,
                          const minimodel::McTrack *gamma1
    );

    void UpdateDecayVertexStats(const minimodel::McTrack *track);

    double CalculateTheta(double px, double py, double pz);
};

#endif   // OPEN_CHARM_PROCESSOR_HEADER