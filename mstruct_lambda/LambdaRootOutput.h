#ifndef MESON_STRUCTURE_LAMBDA_ROOT_OUTPUT_HEADER
#define MESON_STRUCTURE_LAMBDA_ROOT_OUTPUT_HEADER

#include <TFile.h>
#include <TDirectory.h>
#include <TH1F.h>
#include <TH2D.h>
#include <TCanvas.h>
#include <TTree.h>
#include <TApplication.h>

class LambdaRootOutput
{
public:
    void init(TFile *file)
    {
        // 'locker' locks mutex so other threads can't interfere with TFile doing its job
        std::lock_guard<std::recursive_mutex> locker(lock);

        // create a subdirectory "hist_dir" in this file
        plugin_root_dir = file->mkdir("mstruct_lambda");
        file->cd();         // Just in case some other root file is the main TDirectory now
        plugin_root_dir->cd();

        // TTree with recoiled electron
        tree_rec_e = new TTree("rec_e", "a Tree with vect");
        tree_rec_e->SetDirectory(plugin_root_dir);

        //=====================================================================

        //------------------ L A M B D A -------------------------------
        // Lambda
        h1_lam_m_ppi = new TH1F("lam_ppim_mass", "p + #pi^{--} mass no cuts", 100,1.11,1.12);
        h1_lam_m_ppi->GetXaxis()->SetTitle("M [GeV^2]");

        h1_lam_n2g_mass = new TH1F("lam_n2g_mass", " p + #pi^{0}_{#gamma #gamma} mass no cuts", 100,1.11,1.12);
        h1_lam_n2g_mass->GetXaxis()->SetTitle("M [GeV^2]");

        h1_lam_npi0_mass = new TH1F("lam_npi0_mass", " p + #pi^{0} mass for not decayed #pi^{0}", 100,1.11,1.12);
        h1_lam_npi0_mass->GetXaxis()->SetTitle("M [GeV^2]");

        h1_lam_p_decay_z = new TH1F("lam_p_decay_z", "#Lambda #rightarrow p + #pi^{--} decay z", 200, 0,33);
        h1_lam_p_decay_z->GetXaxis()->SetTitle("Z [m]");

        h1_lam_n_decay_z = new TH1F("lam_n_decay_z", "#Lambda #rightarrow n + #pi^{0} decay z", 200, 0,33);
        h1_lam_n_decay_z->GetXaxis()->SetTitle("Z [m]");

        h1_lam_decay_z = new TH1F("lam_decay_z", "#Lambda decay z (all decays)", 200,0,33);
        h1_lam_decay_z->GetXaxis()->SetTitle("Z [m]");

        h2_lam_armenteros = new TH2F("lam_armenteros", "#Lambda #rightarrow p + pi^{--}  Armenteros-Podolansky distribution", 100, -1,1, 100, 0,0.5);
        h2_lam_armenteros->GetXaxis()->SetTitle("#alpha");
        h2_lam_armenteros->GetYaxis()->SetTitle("#P_{T} (GeV/c)");

        h1_lam_p_angle = new TH1F("lam_p_angle", "Angle between #Lambda and p in lab frame", 100, 0, 0.5);
        h1_lam_p_angle->GetXaxis()->SetTitle("angle [deg]");
        h1_lam_n_angle = new TH1F("lam_n_angle", "Angle between #Lambda and n in lab frame", 100, 0, 0.5);
        h1_lam_n_angle->GetXaxis()->SetTitle("angle [deg]");

        h1_lam_pim_angle = new TH1F("lam_pim_angle", "Angle between #Lambda and #pi^{--} in lab frame", 100, 0, 5);
        h1_lam_pim_angle->GetXaxis()->SetTitle("angle [deg]");

        h1_lam_pi0_angle = new TH1F("lam_pi0_angle", "Angle between #Lambda and #pi^{0} in lab frame", 100, 0, 5);
        h1_lam_pi0_angle->GetXaxis()->SetTitle("angle [deg]");

        //------------------ P I   M I N U S ----------------------------------
        h1_pim_theta = new TH1F("pim_theta", "Theta angle (global lab frame) of #pi^{-} (not decayed)", 100, 0, 50);
        h1_pim_theta->GetXaxis()->SetTitle("Theta [mrad]");

        h1_pim_etot= new TH1F("pim_etot", " #pi^{-} total energy", 200, 0., 50.);
        h1_pim_etot->GetXaxis()->SetTitle("E, GeV");

        h1_pim_p = new TH1F("pim_p", " Total momentum of #pi^{-}  (not decayed)", 100, 0., 100.);
        h1_pim_p->GetXaxis()->SetTitle("Momentum, GeV");

        h1_pim_xl = new TH1F("pim_xl", "#pi^{-} Xl", 50, 0., 1.);
        h1_pim_xl->GetXaxis()->SetTitle("Xl");

        // ------------------ PI 0 ----------------------
        h1_pi0_etot= new TH1F("pi0_etot", "Total energy of #pi^{0} (not decayed)", 500, 0., 100.);
        h1_pi0_etot->GetXaxis()->SetTitle("E [GeV]");

        h1_pi0_theta = new TH1F("pi0_theta", "Theta angle (global lab frame) of #pi^{0} (not decayed)", 100, 0, 50);
        h1_pi0_theta->GetXaxis()->SetTitle("Theta [mrad]");

        h1_pi0_p = new TH1F("pi0_p", " Total momentum of #pi^{0}  (not decayed)", 100, 0., 100.);
        h1_pi0_p->GetXaxis()->SetTitle("p [GeV]");

        h1_pi0_xl = new TH1F("pi0_xl", "#pi^{0} (not decayed) Xl", 50, 0., 1.);
        h1_pi0_xl->GetXaxis()->SetTitle("Xl");

        h1_ggpi0_mass = new TH1F("ggpi0_mass", "#pi^{0} #rightarrow #gamma + #gamma mass", 100, 0.13, 0.14);
        h1_ggpi0_mass->GetXaxis()->SetTitle("M [GeV^2]");

        h1_ggpi0_etot= new TH1F("ggpi0_etot", "#pi^{0} #rightarrow #gamma + #gamma total energy", 200, 0., 40.);
        h1_ggpi0_etot->GetXaxis()->SetTitle("E [GeV]");

        h1_ggpi0_theta = new TH1F("ggpi0_theta", "#pi^{0} #rightarrow #gamma + #gamma theta", 100, 0, 50);
        h1_ggpi0_theta->GetXaxis()->SetTitle("Theta [mrad]");

        h1_ggpi0_p = new TH1F("ggpi0_p", "#pi^{0} #rightarrow #gamma + #gamma momentum", 100, 0., 40.);
        h1_ggpi0_p->GetXaxis()->SetTitle("p [GeV]");

        h1_ggpi0_xl = new TH1F("ggpi0_xl", "#pi^{0} #rightarrow #gamma + #gamma Xl", 50, 0., 1.);
        h1_ggpi0_xl->GetXaxis()->SetTitle("Xl");

        // ------------ Selected gamma ---------------------------------
        h1_gamma_etot= new TH1F("gamma_etot", "#gamma energy", 200, 0., 60.);
        h1_gamma_etot->GetXaxis()->SetTitle("E [GeV]");

        h1_gamma_theta = new TH1F("gamma_theta", "#gamma theta", 100, 0., 90.);
        h1_gamma_theta->GetXaxis()->SetTitle("Theta [deg]");

        h1_gamma_p = new TH1F("gamma_p", " Total momentum of #gamma", 100, 0., 40.);
        h1_gamma_p->GetXaxis()->SetTitle("p [GeV]");

        //------------------- Neutron -------------------------------
        h1_n_etot = new TH1F("n_etot", "Total momentum of neutron", 100,0.,100.);
        h1_n_etot->GetXaxis()->SetTitle("E [GeV]");

        h1_n_p = new TH1F("n_p", "Total momentum of neutron", 100,0.,100.);
        h1_n_p->GetXaxis()->SetTitle("p [GeV]");

        h1_n_xl = new TH1F("n_xl", "Xl of neutron", 50, 0, 1);
        h1_n_xl->GetXaxis()->SetTitle("Xl");

        h1_n_pt = new TH1F("n_pt", " Pt of neutron", 200, 0, 10);
        h1_n_pt->GetXaxis()->SetTitle("pt [GeV]");

        h1_n_theta = new TH1F("n_theta", " Theta of neutron", 100, 0, 40);
        h1_n_theta->GetXaxis()->SetTitle("Theta [mrad]");

        //------------------- Proton -------------------------------
        h1_prot_etot = new TH1F("prot_etot", "Total momentum of neutron", 100,0.,100.);
        h1_prot_etot->GetXaxis()->SetTitle("E [GeV]");

        h1_prot_p = new TH1F("prot_p", "Total momentum of neutron", 100,0.,100.);
        h1_prot_p->GetXaxis()->SetTitle("p [GeV]");

        h1_prot_xl = new TH1F("prot_xl", "Xl of neutron", 50, 0, 1);
        h1_prot_xl->GetXaxis()->SetTitle("Xl");

        h1_prot_pt = new TH1F("prot_pt", " Pt of neutron", 200, 0, 10);
        h1_prot_pt->GetXaxis()->SetTitle("pt [GeV]");

        h1_prot_theta = new TH1F("prot_theta", " Theta of neutron", 100, 0, 40);
        h1_prot_theta->GetXaxis()->SetTitle("Theta [mrad]");

        // ================== H I T S ====================
        h2_xy_hits_barrel = new TH2I("gam_xy_hits_barrel", "Gamma X,Y hits in Ion Endcap EMCAL", 200, -2000, 2000, 200, -2000, 2000);
        h2_xy_hits_zdc = new TH2I("gam_xy_hits_zdc", "Gamma X,Y hits in FFI ZDC", 70, 500, 1300, 70, -400, 400);
        h2_xy_hits_ioncap = new TH2I("gam_xy_hits_ioncap", "Gamma X,Y hits in Central Barrel EMCAL", 200, -2000, 2000, 200, -2000, 2000);
        h1_z_hits_barrel = new TH1I("gam_z_hits_barrel", "Gamma Z hits in Central Barrel EMCAL", 100, -3000, 3000);
        h1_z_hits_any =    new TH1I("gam_z_hits_any", "Gamma Z hits in any EMCAL", 200, -6000, 6000);

        h1_lam_z_hits_any = new TH1F("lam_z_hits_any", "#Lambda Z hits in any detector", 200, 0, 30);
        h2_n_xy_hits_zdc = new TH2I("n_xy_hits_zdc", "Neutron X,Y hits in ZDC", 70, 500, 1300, 70, -400, 400);
        h1_n_z_hits_zdc  = new TH1I("n_z_hits_any", "Z hits in any EMCAL [m]", 200, 0, 85);     // Z plots for Barrel ZDC
    }

    // ---- Lambda decay ----
    TH1F *h1_lam_m_ppi;
    TH1F *h1_lam_p_decay_z;
    TH1F *h1_lam_n_decay_z;
    TH1F *h1_lam_decay_z;

    TH1F *h1_lam_p_angle;
    TH1F *h1_lam_n_angle;
    TH1F *h1_lam_pim_angle;
    TH1F *h1_lam_pi0_angle;

    TH1F *h1_ggpi0_mass;
    TH1F *h1_lam_n2g_mass;
    TH1F *h1_lam_npi0_mass;

    TH2F *h2_lam_armenteros;

    // Pi 0
    TH1F *h1_pi0_theta;
    TH1F *h1_pi0_etot;
    TH1F *h1_pi0_p;
    TH1F *h1_pi0_xl;

    TH1F *h1_ggpi0_theta;
    TH1F *h1_ggpi0_etot;
    TH1F *h1_ggpi0_p;
    TH1F *h1_ggpi0_xl;

    // Pi minus
    TH1F *h1_pim_theta;
    TH1F *h1_pim_etot;
    TH1F *h1_pim_p;
    TH1F *h1_pim_xl;

    // Neutron
    TH1F *h1_n_theta;
    TH1F *h1_n_etot;
    TH1F *h1_n_p;
    TH1F *h1_n_pt;
    TH1F *h1_n_xl;

    // Proton
    TH1F *h1_prot_theta;
    TH1F *h1_prot_etot;
    TH1F *h1_prot_p;
    TH1F *h1_prot_pt;
    TH1F *h1_prot_xl;

    // Gamma
    TH1F* h1_gamma_etot;
    TH1F* h1_gamma_theta;
    TH1F* h1_gamma_p;


    // ---- hits ----
    TH2I *h2_xy_hits_barrel;    // X,Y plots for Barrel EMCAL
    TH2I *h2_xy_hits_zdc;     // X,Y plots for Electron endcap EMCAL
    TH2I *h2_xy_hits_ioncap;    // X,Y plots for ION endcap EMCAL
    TH1I *h1_z_hits_barrel;     // Z plots for Barrel EMCAL
    TH1I *h1_z_hits_any;        // Z plots for all EMCALs

    TH1F *h1_lam_z_hits_any;        // Z plots for all EMCALs

    TH2I *h2_n_xy_hits_zdc;    // X,Y plots for ION endcap ZDC
    TH1I *h1_n_z_hits_zdc;     // Z plots for Barrel ZDC

    std::recursive_mutex lock;

    TTree * tree_rec_e;     // Tree to store electron related data

private:

    TDirectory* plugin_root_dir;   // Main TDirectory for Plugin histograms and data

};

#endif // OPEN_CHARM_ROOT_OUTPUT_HEADER