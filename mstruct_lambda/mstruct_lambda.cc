#include <JANA/JFactoryGenerator.h>

#include "LambdaProcessor.h"

extern "C"
{
    void InitPlugin(JApplication *app)
    {
        InitJANAPlugin(app);

        app->Add(new LambdaProcessor(app));        
    }
}
