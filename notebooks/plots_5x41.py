# To add a new cell, type ''
# To add a new markdown cell, type '# %% [markdown]'

# %%
import uproot4
import hist
import matplotlib.pyplot as plt
import uproot4.dynamic
import mplhep as hep

plt.style.use(hep.style.fabiola)  # or ATLAS/LHCb
# %%
file = uproot4.open("lambda_ana_5on41_all_l3.root")


# %%
# https://matplotlib.org/api/_as_gen/matplotlib.pyplot.fill_between.html

def plot_1d_hist(root_hist, label="", ax=None, histtype='fill', alpha=1):
    #fig, ax = plt.subplots(figsize=(6, 4))
    h = root_hist.to_hist()
    h.axes.label = root_hist.all_members['fXaxis'].all_members["fTitle"]    
    ax = h.plot(ax=ax, histtype='fill', alpha=alpha)
    print(type(ax))
    #ax = h.plot(ax=ax, histtype='step')
    #return ax
# %%
ax = plot_1d_hist(file['mstruct_lambda/lam_decay_z'], alpha=0.5)
plot_1d_hist(file['mstruct_lambda/lam_n_decay_z'], ax=ax, alpha=0.5)
# %%

plot_1d_hist(file['mstruct_lambda/lam_decay_z'], alpha=0.5)

"""
'mstruct_lambda',
 'mstruct_lambda/rec_e',
 'mstruct_lambda/lam_ppim_mass',
 'mstruct_lambda/lam_n2g_mass',
 'mstruct_lambda/lam_npi0_mass',
 'mstruct_lambda/lam_p_decay_z',
 'mstruct_lambda/lam_n_decay_z',
 'mstruct_lambda/lam_decay_z',
 'mstruct_lambda/lam_armenteros',
 'mstruct_lambda/lam_p_angle',
 'mstruct_lambda/lam_n_angle',
 'mstruct_lambda/lam_pim_angle',
 'mstruct_lambda/lam_pi0_angle',
 'mstruct_lambda/pi0_etot',
 'mstruct_lambda/pi0_theta',
 'mstruct_lambda/pi0_p',
 'mstruct_lambda/ggpi0_mass',
 'mstruct_lambda/ggpi0_etot',
 'mstruct_lambda/ggpi0_theta',
 'mstruct_lambda/ggpi0_p',
 'mstruct_lambda/gamma_etot',
 'mstruct_lambda/gamma_theta',
 'mstruct_lambda/gamma_p',
 'mstruct_lambda/n_p',
 'mstruct_lambda/n_etot',
 'mstruct_lambda/n_pt',
 'mstruct_lambda/n_theta',
 'mstruct_lambda/gam_xy_hits_barrel',
 'mstruct_lambda/gam_xy_hits_zdc',
 'mstruct_lambda/gam_xy_hits_ioncap',
 'mstruct_lambda/gam_z_hits_barrel',
 'mstruct_lambda/gam_z_hits_any',
 'mstruct_lambda/n_xy_hits_zdc',
 'mstruct_lambda/n_z_hits_any;1'
"""