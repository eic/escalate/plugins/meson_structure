def parse_event_count(file_name):
    bufsize = 1000000
    event_counter = 0

    with open(file_name) as infile:
        while True:
            lines = infile.readlines(bufsize)
            if not lines:
                break
            for i, line in enumerate(lines):
                if len(line.split()) == 10:
                    event_counter += 1
    return event_counter


"""
'OUTPUTS/g4e-lvl1-k_lambda_10on100'

# Run g4e run!!!
g4e.source('/w/general-scifs17exp/eic/mc/meson_MC/OUTPUTS/k_lambda_10on100_lund.dat') \
"""

# The files look like this
# /home/romanov/eic/data/k_lambda_5on41_lund.dat
# /home/romanov/eic/data/k_lambda_5on100_lund.dat
# /home/romanov/eic/data/k_lambda_10on100_lund.dat
# /home/romanov/eic/data/k_lambda_18on275_lund.dat


energy_pairs = [(5, 41), (5, 100), (10, 100), (10, 135)]
#energy_pairs = [(5, 41), (5, 100), (10, 100)]
#energy_pairs = [(10, 135)]

for e_energy, i_energy in energy_pairs:

    input_file_name = '/home/romanov/jlab/data/k_lambda_{}on{}_x0.001-1.000_q1.0-100.0_lund.dat'.format(e_energy, i_energy)
    output_name = 'OUTPUTS/g4e_k_lambda_{}on{}_all_l3'.format(e_energy, i_energy)
    nevents = parse_event_count(input_file_name)

    # Pretty printing the results
    print(f"\n\n DO ENERGIES {e_energy} on {i_energy}")
    print("====================================")
    print(f"input_file_name = {input_file_name}")
    print(f"output_name     = {output_name}")
    print(f"nevents         = {nevents}")
    print("\n\n\n")




