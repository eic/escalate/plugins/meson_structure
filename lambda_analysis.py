from pyjano.jana import Jana, PluginFromSource

mstruct_lambda = PluginFromSource('./mstruct_lambda')   # Name will be determined from folder name
# add name=<...> for custom name

jana = Jana(nevents=50000, output='OUTPUTS/lambda_analysis.root')

# G4E reader here
jana.plugin('g4e_reader') \
    .source('OUTPUTS/g4e_k_lambda_18on275_all_l3.root')

# Parameters:
#     verbose   - Plugin output level. 0-almost nothing, 1-some, 2-everything
#     smearing  - Particle smearing 0-true MC, 1-smearing, 2-reconstruction");
# Beams energies. Defaults are 10x100 GeV
#     e_beam_energy    -  Energy of colliding electron beam");
#     ion_beam_energy  -  Energy of colliding ion beam");
jana.plugin(mstruct_lambda, verbose=1)

jana.run()